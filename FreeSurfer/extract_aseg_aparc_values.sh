#!/bin/bash

SUBJECTS_DIR=/data/pt_02571/Data/preprocessed/freesurfer/

# asegstats2table & aparcstats2table
cd $SUBJECTS_DIR

subjlist=$(ls -d LI* | cut -c 1-10)


# Run asegstats2table command.
#asegstats2table --subjects $subjlist --skip --meas volume --tablefile /data/pt_02571/Results/MRI/FREESURFER/aseg_stats_lc.txt

# Run aparcstats2table command.
aparcstats2table --subjects $subjlist --hemi rh --skip --meas thickness --tablefile /data/pt_02571/Results/MRI/FREESURFER/aparc_stats_rh.txt
aparcstats2table --subjects $subjlist --hemi lh --skip --meas thickness --tablefile /data/pt_02571/Results/MRI/FREESURFER/aparc_stats_lh.txt

