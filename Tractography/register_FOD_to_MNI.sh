#Register FOD image to MNI space like in Carandini et al. 

#First extract first volume of WM FOD template
mrconvert /data/pt_02571/Data/preprocessed/template/wmfod_template.mif -coord 3 0 -axes 0,1,2 /data/pt_02571/Data/preprocessed/template/wmfod_template_dir0.nii

#Perform antsRegistration from FOD to MNI space using default parameters.
antsRegistration --collapse-output-transforms 1 --dimensionality 3 --initial-moving-transform [ /afs/cbs.mpg.de/software/fsl/6.0.3/debian-bullseye-amd64/data/standard/MNI152_T1_1mm_brain.nii.gz, /data/pt_02571/Data/preprocessed/template/wmfod_template_dir0.nii, 1 ] --initialize-transforms-per-stage 0 --interpolation Linear --output [ transform, transform_Warped.nii.gz, transform_InverseWarped.nii.gz ] --transform Rigid[ 0.1 ] --metric MI[ /afs/cbs.mpg.de/software/fsl/6.0.3/debian-bullseye-amd64/data/standard/MNI152_T1_1mm_brain.nii.gz, /data/pt_02571/Data/preprocessed/template/wmfod_template_dir0.nii, 1, 32, Regular, 0.25 ] --convergence [ 1000x500x250x100, 1e-06, 10 ] --smoothing-sigmas 3.0x2.0x1.0x0.0vox --shrink-factors 8x4x2x1 --use-histogram-matching 1 --transform Affine[ 0.1 ] --metric MI[ /afs/cbs.mpg.de/software/fsl/6.0.3/debian-bullseye-amd64/data/standard/MNI152_T1_1mm_brain.nii.gz, /data/pt_02571/Data/preprocessed/template/wmfod_template_dir0.nii, 1, 32, Regular, 0.25 ] --convergence [ 1000x500x250x100, 1e-06, 10 ] --smoothing-sigmas 3.0x2.0x1.0x0.0vox --shrink-factors 8x4x2x1 --use-histogram-matching 1 --transform SyN[ 0.1, 3.0, 0.0 ] --metric CC[ /afs/cbs.mpg.de/software/fsl/6.0.3/debian-bullseye-amd64/data/standard/MNI152_T1_1mm_brain.nii.gz, /data/pt_02571/Data/preprocessed/template/wmfod_template_dir0.nii, 1, 4, None, 1 ] --convergence [ 100x70x50x20, 1e-06, 10 ] --smoothing-sigmas 3.0x2.0x1.0x0.0vox --shrink-factors 8x4x2x1 --use-histogram-matching 1 --winsorize-image-intensities [ 0.005, 0.995 ]  --write-composite-transform 0

#Apply transform to 
/afs/cbs.mpg.de/software/fsl/6.0.3/debian-bullseye-amd64/data/atlases/Cerebellum/Cerebellum-MNIfnirt-maxprob-thr25-1mm.nii.gz
/afs/cbs.mpg.de/software/fsl/6.0.3/debian-bullseye-amd64/data/atlases/HarvardOxford/HarvardOxford-cort-maxprob-thr25-1mm.nii.gz
/afs/cbs.mpg.de/software/fsl/6.0.3/debian-bullseye-amd64/data/atlases/HarvardOxford/HarvardOxford-sub-maxprob-thr25-1mm.nii.gz


#From Brainstem Navigator:
#locus coereleus
LC_l, LC_r

#median raphe
MnR

#dorsal raphe
DR

/data/pt_02571/Data/Tractography/BrainstemNavigator/BrainstemNavigator/0.9/2a.BrainstemNucleiAtlas_MNI/labels_thresholded_binary_0.35/DR.nii.gz
/data/pt_02571/Data/Tractography/BrainstemNavigator/BrainstemNavigator/0.9/2a.BrainstemNucleiAtlas_MNI/labels_thresholded_binary_0.35/LC_l.nii.gz
/data/pt_02571/Data/Tractography/BrainstemNavigator/BrainstemNavigator/0.9/2a.BrainstemNucleiAtlas_MNI/labels_thresholded_binary_0.35/LC_r.nii.gz
/data/pt_02571/Data/Tractography/BrainstemNavigator/BrainstemNavigator/0.9/2a.BrainstemNucleiAtlas_MNI/labels_thresholded_binary_0.35/MnR.nii.gz


#AntsApplyTransform
#test:
antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input /afs/cbs.mpg.de/software/fsl/6.0.3/debian-bullseye-amd64/data/standard/MNI152_T1_1mm_brain.nii.gz --input-image-type 3 --interpolation Linear --output /data/pt_02571/Data/Tractography/Atlas2FOD/MNI_template_warped.nii.gz --reference-image /data/pt_02571/Data/preprocessed/template/wmfod_template_dir0.nii --transform /data/pt_02571/Data/preprocessed/template/transform2MNI/transform1InverseWarp.nii.gz --transform [/data/pt_02571/Data/preprocessed/template/transform2MNI/transform0GenericAffine.mat,1]

#apply to cerebellum labels:
antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input /afs/cbs.mpg.de/software/fsl/6.0.3/debian-bullseye-amd64/data/atlases/Cerebellum/Cerebellum-MNIfnirt-maxprob-thr25-1mm.nii.gz --input-image-type 3 --interpolation MultiLabel --output /data/pt_02571/Data/Tractography/Atlas2FOD/Cerebellum-MNIfnirt-maxprob-thr25-1mm_warped.nii.gz --reference-image /data/pt_02571/Data/preprocessed/template/wmfod_template_dir0.nii --transform /data/pt_02571/Data/preprocessed/template/transform2MNI/transform1InverseWarp.nii.gz --transform [/data/pt_02571/Data/preprocessed/template/transform2MNI/transform0GenericAffine.mat,1]

#apply to Harvard-Oxford cort and subcort labels
antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input /afs/cbs.mpg.de/software/fsl/6.0.3/debian-bullseye-amd64/data/atlases/HarvardOxford/HarvardOxford-cort-maxprob-thr25-1mm.nii.gz --input-image-type 3 --interpolation MultiLabel --output /data/pt_02571/Data/Tractography/Atlas2FOD/HarvardOxford-cort-maxprob-thr25-1mm_warped.nii.gz --reference-image /data/pt_02571/Data/preprocessed/template/wmfod_template_dir0.nii --transform /data/pt_02571/Data/preprocessed/template/transform2MNI/transform1InverseWarp.nii.gz --transform [/data/pt_02571/Data/preprocessed/template/transform2MNI/transform0GenericAffine.mat,1]

antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input /afs/cbs.mpg.de/software/fsl/6.0.3/debian-bullseye-amd64/data/atlases/HarvardOxford/HarvardOxford-sub-maxprob-thr25-1mm.nii.gz --input-image-type 3 --interpolation MultiLabel --output /data/pt_02571/Data/Tractography/Atlas2FOD/HarvardOxford-sub-maxprob-thr25-1mm_warped.nii.gz --reference-image /data/pt_02571/Data/preprocessed/template/wmfod_template_dir0.nii --transform /data/pt_02571/Data/preprocessed/template/transform2MNI/transform1InverseWarp.nii.gz --transform [/data/pt_02571/Data/preprocessed/template/transform2MNI/transform0GenericAffine.mat,1]

#apply to Brainstem Nuclei
antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input /data/pt_02571/Data/Tractography/BrainstemNavigator/BrainstemNavigator/0.9/2a.BrainstemNucleiAtlas_MNI/labels_thresholded_binary_0.35/MnR.nii.gz --input-image-type 3 --interpolation NearestNeighbor --output /data/pt_02571/Data/Tractography/Atlas2FOD/MnR_warped.nii.gz --reference-image /data/pt_02571/Data/preprocessed/template/wmfod_template_dir0.nii --transform /data/pt_02571/Data/preprocessed/template/transform2MNI/transform1InverseWarp.nii.gz --transform [/data/pt_02571/Data/preprocessed/template/transform2MNI/transform0GenericAffine.mat,1]

antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input /data/pt_02571/Data/Tractography/BrainstemNavigator/BrainstemNavigator/0.9/2a.BrainstemNucleiAtlas_MNI/labels_thresholded_binary_0.35/DR.nii.gz --input-image-type 3 --interpolation NearestNeighbor --output /data/pt_02571/Data/Tractography/Atlas2FOD/DR_warped.nii.gz --reference-image /data/pt_02571/Data/preprocessed/template/wmfod_template_dir0.nii --transform /data/pt_02571/Data/preprocessed/template/transform2MNI/transform1InverseWarp.nii.gz --transform [/data/pt_02571/Data/preprocessed/template/transform2MNI/transform0GenericAffine.mat,1]

#LC summed 
fslmaths /data/pt_02571/Data/Tractography/BrainstemNavigator/BrainstemNavigator/0.9/2a.BrainstemNucleiAtlas_MNI/labels_thresholded_binary_0.35/LC_l.nii.gz -add /data/pt_02571/Data/Tractography/BrainstemNavigator/BrainstemNavigator/0.9/2a.BrainstemNucleiAtlas_MNI/labels_thresholded_binary_0.35/LC_r.nii.gz /data/pt_02571/Data/Tractography/Atlas2FOD/LC_added.nii.gz

#LC -> too small to appear when using MultiLabel Interpolation.
antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input /data/pt_02571/Data/Tractography/Atlas2FOD/LC_added.nii.gz --input-image-type 3 --interpolation NearestNeighbor --output /data/pt_02571/Data/Tractography/Atlas2FOD/LC_warped.nii.gz --reference-image /data/pt_02571/Data/preprocessed/template/wmfod_template_dir0.nii --transform /data/pt_02571/Data/preprocessed/template/transform2MNI/transform1InverseWarp.nii.gz --transform [/data/pt_02571/Data/preprocessed/template/transform2MNI/transform0GenericAffine.mat,1]

#VTA summed 
fslmaths /data/pt_02571/Data/Tractography/BrainstemNavigator/BrainstemNavigator/0.9/2a.BrainstemNucleiAtlas_MNI/labels_thresholded_binary_0.35/VTA_PBP_l.nii.gz -add /data/pt_02571/Data/Tractography/BrainstemNavigator/BrainstemNavigator/0.9/2a.BrainstemNucleiAtlas_MNI/labels_thresholded_binary_0.35/VTA_PBP_r.nii.gz /data/pt_02571/Data/Tractography/Atlas2FOD/VTA_PBC_added.nii.gz

antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input /data/pt_02571/Data/Tractography/Atlas2FOD/VTA_PBC_added.nii.gz --input-image-type 3 --interpolation NearestNeighbor --output /data/pt_02571/Data/Tractography/Atlas2FOD/VTA_PBC_warped.nii.gz --reference-image /data/pt_02571/Data/preprocessed/template/wmfod_template_dir0.nii --transform /data/pt_02571/Data/preprocessed/template/transform2MNI/transform1InverseWarp.nii.gz --transform [/data/pt_02571/Data/preprocessed/template/transform2MNI/transform0GenericAffine.mat,1]

#Transform WM tracts
antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input /afs/cbs.mpg.de/software/fsl/6.0.3/debian-bullseye-amd64/data/atlases/JHU/JHU-ICBM-tracts-maxprob-thr25-1mm.nii.gz --input-image-type 3 --interpolation NearestNeighbor --output /data/pt_02571/Data/Tractography/Atlas2FOD/JHU-ICBM-tracts-maxprob-thr25-1mm_warped.nii.gz --reference-image /data/pt_02571/Data/preprocessed/template/wmfod_template_dir0.nii --transform /data/pt_02571/Data/preprocessed/template/transform2MNI/transform1InverseWarp.nii.gz --transform [/data/pt_02571/Data/preprocessed/template/transform2MNI/transform0GenericAffine.mat,1]

