#!/bin/bash
#
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --mem=5000
#SBATCH --time=0-01:00
#SBATCH --job-name LI02044730_erode
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-type=fail
#SBATCH --mail-user=mangels@cbs.mpg.de
#SBATCH -o /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/erode_slurm/LI02044730_erode.out
#SBATCH -e /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/erode_slurm/LI02044730_erode.err

fslmaths /data/pt_02571/Data/preprocessed/nipype/diffusion/LI02044730/eddy_dwi_mask_upsampled.nii.gz -ero -bin /data/pt_02571/Data/preprocessed/nipype/diffusion/LI02044730/eroded_mask_upsampled.nii.gz

fslmaths /data/pt_02571/Data/preprocessed/nipype/diffusion/LI02044730/eroded_mask_upsampled.nii.gz -ero -bin /data/pt_02571/Data/preprocessed/nipype/diffusion/LI02044730/2nd_eroded_mask_upsampled.nii.gz

mrconvert /data/pt_02571/Data/preprocessed/nipype/diffusion/LI02044730/2nd_eroded_mask_upsampled.nii.gz /data/pt_02571/Data/preprocessed/nipype/diffusion/LI02044730/2nd_eroded_mask_upsampled.mif -force

