#!/bin/bash
#
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --mem=5000
#SBATCH --time=0-01:00
#SBATCH --job-name LI08781694_erode
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-type=fail
#SBATCH --mail-user=mangels@cbs.mpg.de
#SBATCH -o /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/erode_slurm/LI08781694_erode.out
#SBATCH -e /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/erode_slurm/LI08781694_erode.err

fslmaths /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08781694/eddy_dwi_mask_upsampled.nii.gz -ero -bin /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08781694/eroded_mask_upsampled.nii.gz

fslmaths /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08781694/eroded_mask_upsampled.nii.gz -ero -bin /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08781694/2nd_eroded_mask_upsampled.nii.gz

mrconvert /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08781694/2nd_eroded_mask_upsampled.nii.gz /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08781694/2nd_eroded_mask_upsampled.mif -force

