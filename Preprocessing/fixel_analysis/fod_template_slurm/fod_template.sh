#!/bin/bash
#
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --mem=34000
#SBATCH --time=1-10:00
#SBATCH --job-name fod_temp
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-type=fail
#SBATCH --mail-user=mangels@cbs.mpg.de
#SBATCH -o /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/fod_template_slurm/fod_temp.out
#SBATCH -e /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/fod_template_slurm/fod_temp.err

population_template /data/pt_02571/Data/preprocessed/template/fod_input -mask_dir /data/pt_02571/Data/preprocessed/template/mask_input /data/pt_02571/Data/preprocessed/template/wmfod_template.mif -voxel_size 1.25 -force

