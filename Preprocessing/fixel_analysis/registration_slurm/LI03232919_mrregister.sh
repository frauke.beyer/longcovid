#!/bin/bash
#
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --mem=16000
#SBATCH --time=0-02:00
#SBATCH --job-name LI03232919_reg
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-type=fail
#SBATCH --mail-user=mangels@cbs.mpg.de
#SBATCH -o /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/registration_slurm/LI03232919_registration.out
#SBATCH -e /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/registration_slurm/LI03232919_registration.err

mrregister /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03232919/wmfod_norm.mif -mask1 /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03232919/eddy_dwi_mask_upsampled.mif /data/pt_02571/Data/preprocessed/template/wmfod_template.mif -nl_warp /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03232919/subject2template_warp.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03232919/template2subject_warp.mif -force

