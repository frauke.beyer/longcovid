#!/bin/bash
#
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --mem=16000
#SBATCH --time=0-02:00
#SBATCH --job-name LI04105215_mtnorm
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-type=fail
#SBATCH --mail-user=mangels@cbs.mpg.de
#SBATCH -o /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/mtnorm_slurm/LI04105215_mtnorm.out
#SBATCH -e /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/mtnorm_slurm/LI04105215_mtnorm.err
mtnormalise /data/pt_02571/Data/preprocessed/nipype/diffusion/LI04105215/wmfod.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI04105215/wmfod_norm.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI04105215/gm.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI04105215/gm_norm.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI04105215/csf.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI04105215/csf_norm.mif -mask /data/pt_02571/Data/preprocessed/nipype/diffusion/LI04105215/2nd_eroded_mask_upsampled.mif -force

