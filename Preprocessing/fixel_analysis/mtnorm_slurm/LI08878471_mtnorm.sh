#!/bin/bash
#
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --mem=16000
#SBATCH --time=0-02:00
#SBATCH --job-name LI08878471_mtnorm
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-type=fail
#SBATCH --mail-user=mangels@cbs.mpg.de
#SBATCH -o /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/mtnorm_slurm/LI08878471_mtnorm.out
#SBATCH -e /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/mtnorm_slurm/LI08878471_mtnorm.err
mtnormalise /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08878471/wmfod.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08878471/wmfod_norm.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08878471/gm.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08878471/gm_norm.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08878471/csf.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08878471/csf_norm.mif -mask /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08878471/2nd_eroded_mask_upsampled.mif -force

