#!/bin/bash
#
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --mem=16000
#SBATCH --time=0-02:00
#SBATCH --job-name LI03232919_mtnorm
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-type=fail
#SBATCH --mail-user=mangels@cbs.mpg.de
#SBATCH -o /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/mtnorm_slurm/LI03232919_mtnorm.out
#SBATCH -e /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/mtnorm_slurm/LI03232919_mtnorm.err
mtnormalise /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03232919/wmfod.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03232919/wmfod_norm.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03232919/gm.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03232919/gm_norm.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03232919/csf.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03232919/csf_norm.mif -mask /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03232919/2nd_eroded_mask_upsampled.mif -force

