#!/bin/bash
#
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --mem=16000
#SBATCH --time=0-02:00
#SBATCH --job-name LI02050539_mtnorm
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-type=fail
#SBATCH --mail-user=mangels@cbs.mpg.de
#SBATCH -o /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/mtnorm_slurm/LI02050539_mtnorm.out
#SBATCH -e /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/mtnorm_slurm/LI02050539_mtnorm.err
mtnormalise /data/pt_02571/Data/preprocessed/nipype/diffusion/LI02050539/wmfod.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI02050539/wmfod_norm.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI02050539/gm.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI02050539/gm_norm.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI02050539/csf.mif /data/pt_02571/Data/preprocessed/nipype/diffusion/LI02050539/csf_norm.mif -mask /data/pt_02571/Data/preprocessed/nipype/diffusion/LI02050539/2nd_eroded_mask_upsampled.mif -force

