#!/bin/bash
#
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --mem=8000
#SBATCH --time=0-02:00
#SBATCH --job-name LI08796757_verio_ss3t
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-type=fail
#SBATCH --mail-user=mangels@cbs.mpg.de
#SBATCH -o /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/ss3t_slurm/LI08796757_verio_ss3t.out
#SBATCH -e /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/ss3t_slurm/LI08796757_verio_33t.err

ss3t_csd_beta1 /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08796757/eddy_corrected_unbiased_upsampled.mif /data/pt_02571/Data/preprocessed/fixel_analysis/verio_group_average_response_wm.txt /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08796757/wmfod.mif /data/pt_02571/Data/preprocessed/fixel_analysis/verio_group_average_response_gm.txt /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08796757/gm.mif  /data/pt_02571/Data/preprocessed/fixel_analysis/verio_group_average_response_csf.txt /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08796757/csf.mif -mask /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08796757/eddy_dwi_mask_upsampled.mif -force

