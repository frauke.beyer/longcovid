#!/bin/bash
#
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --mem=16000
#SBATCH --time=0-02:00
#SBATCH --job-name LI03232919_link
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-type=fail
#SBATCH --mail-user=mangels@cbs.mpg.de
#SBATCH -o /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/template_links_slurm/LI03232919_mtnorm.out
#SBATCH -e /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/template_links_slurm/LI03232919_mtnorm.err
ln -sr /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03232919/wmfod_norm.mif /data/pt_02571/Data/preprocessed/template/fod_input/LI03232919_PRE.mif

ln -sr /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03232919/eddy_dwi_mask_upsampled.mif /data/pt_02571/Data/preprocessed/template/mask_input/LI03232919_PRE.mif

