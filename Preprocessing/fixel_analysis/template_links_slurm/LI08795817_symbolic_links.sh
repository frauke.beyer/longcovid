#!/bin/bash
#
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --mem=16000
#SBATCH --time=0-02:00
#SBATCH --job-name LI08795817_link
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-type=fail
#SBATCH --mail-user=mangels@cbs.mpg.de
#SBATCH -o /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/template_links_slurm/LI08795817_mtnorm.out
#SBATCH -e /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/template_links_slurm/LI08795817_mtnorm.err
ln -sr /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08795817/wmfod_norm.mif /data/pt_02571/Data/preprocessed/template/fod_input/LI08795817_PRE.mif

ln -sr /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08795817/eddy_dwi_mask_upsampled.mif /data/pt_02571/Data/preprocessed/template/mask_input/LI08795817_PRE.mif

