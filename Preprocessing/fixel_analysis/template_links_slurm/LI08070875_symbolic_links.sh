#!/bin/bash
#
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --mem=16000
#SBATCH --time=0-02:00
#SBATCH --job-name LI08070875_link
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-type=fail
#SBATCH --mail-user=mangels@cbs.mpg.de
#SBATCH -o /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/template_links_slurm/LI08070875_mtnorm.out
#SBATCH -e /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/template_links_slurm/LI08070875_mtnorm.err
ln -sr /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08070875/wmfod_norm.mif /data/pt_02571/Data/preprocessed/template/fod_input/LI08070875_PRE.mif

ln -sr /data/pt_02571/Data/preprocessed/nipype/diffusion/LI08070875/eddy_dwi_mask_upsampled.mif /data/pt_02571/Data/preprocessed/template/mask_input/LI08070875_PRE.mif

