#!/bin/bash
#
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --mem=16000
#SBATCH --time=0-02:00
#SBATCH --job-name LI01213470_link
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-type=fail
#SBATCH --mail-user=mangels@cbs.mpg.de
#SBATCH -o /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/template_links_slurm/LI01213470_mtnorm.out
#SBATCH -e /data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis/template_links_slurm/LI01213470_mtnorm.err
ln -sr /data/pt_02571/Data/preprocessed/nipype/diffusion/LI01213470/wmfod_norm.mif /data/pt_02571/Data/preprocessed/template/fod_input/LI01213470_PRE.mif

ln -sr /data/pt_02571/Data/preprocessed/nipype/diffusion/LI01213470/eddy_dwi_mask_upsampled.mif /data/pt_02571/Data/preprocessed/template/mask_input/LI01213470_PRE.mif

