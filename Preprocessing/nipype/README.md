# long covid 

# TO BE UPDATED

Preprocessing pipelines for the 

+ BIDS conversion: copies NIFTI & corresponding .json files into `/data/p_02571/mri/LC*/BIDS/` (functions and subworkflows in: `bids`)

+ Structural preprocessing: with Freesurfer + registration to MNI152 1mm space (functions and subworkflows in: `structural`)

+ Diffusion MRI preprocessing: artefacts correction including denoising (MRTrix: dwidenoise) and Gibb's ringing removal (MRTrix: mrdegibbs); field distortion correction (FSL: topup); motion correction and outliner replacement (FSL: eddy); tensor model fitting (FSL: dtifit)(functions and subworkflows in: `diffusion`)



## How to run the preprocessing workflow

### Prerequisite:
- python 3.9.2 environment with packages listed in `/data/pt_02571/Analysis/longcovid/Preprocessing/requirements.txt` (```lcpy```environment)
- nipype 1.8.5 (pydicom)
- note to self: ```miniconda3``` ```source activate lcpy```

### Step-by-step

1. Connect to compute server `getserver -sL` (not wiesel)
2. load `FREESURFER --version 7.3.2` (which also activates FSL) and `ANTSENV --version 2.3.5` 
3. activate the virtual Python environment (see above) and change input in `run_workflow_XY_nipype_par.py`
4. type ```python run_workflow_XY_nipype_par`.py`` (runs multiple subjects in parallel via nipype functionalities)
5. to run individual subjects on cluster use ```run-as-batch-script-XY.py``
4.  After the workflow has finished
  - check for error logfiles (`*.txt`) and try to solve issues


- The environment may be installed in different ways:
1. use `virtualenv` to create a virtual environment. This is a most basic version control tool for Python 2.7. Detailed instructions can be found [here](https://packaging.python.org/tutorials/installing-packages/).

    - create a directory where your virtualenv should live `/home/user/myvirtualenv`
    - test that you have `pip` installed in your python version by typing `python3 -m pip --version` (should say `pip 9.0.1 from /usr/lib/python2.7/dist-packages (python 2.7)`)
    - install virtualenv to your default institute python 3.9.2 version by typing `python -m pip install virtualenv`
    - create your own virualenvironment by typing `python -m virtualenv /home/user/myvirtualenv`
    - activate this environment by typing `source /home/user/myvirtualenv/bin/activate`
    - now you are using python with this environment and may install all desired packages into it
    - `/data/gh_gr_agingandobesity_share/life_shared/Analysis/MRI/LIFE_followup/preprocessing/nipy1.4/requirements.txt` tells you which packages you need
    - run `python -m pip install -r requirements.txt`
    - now you can use this virtual environment in any session after **activating** it with `source /home/user/myvirtualenv/bin/activate`



  2. use `miniconda` for version control.
    - install `miniconda` into a local directory. For detailed instructions see [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html)
    - create a Python environment using `conda create -y --name myenv python=3.9.2`. This environment will be located in your local conda directory
    - now add the required packages using `conda install --force-reinstall -y -q --name myenv -c conda-forge --file requirements.txt`
    - if not everything can be installed via `conda install`, activate your environment `source activate myenv` and use `pip install -r requirements.txt ` to install the missing packages
    - now you can use this virtual environment in any session after **activating** it with `source activate myenv`





