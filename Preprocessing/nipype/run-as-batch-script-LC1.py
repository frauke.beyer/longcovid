#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  1 10:12:47 2023

@author: mangels
"""
import sys
sys.path.append('/data/pt_life/data_fbeyer/local2/miniconda2/envs/py392/site-packages/')
import os
from workflow import create_workflow
import pandas as pd
import subprocess
'''
Meta script to run preprocessing of long covid LC1
---------------------------------------------------
Can run in two modes:
python run_workflow.py s {subject_id}
python run_workflow.py f {text file containing list of subjects}
'''
# slurm resources
user="mangels"
slurm_dir = "/data/pt_02571/Analysis/longcovid/Preprocessing/nipype/slurm/"
mem_req = 6000 # adjust accordingly
tim_req = "1-00:00" # days-hours:minutes. Current max time is 7 days

# iterate over subjects, one job per subject
# df=pd.read_csv("/data/pt_02571/Data/A1_LC1/QualityCheck_MRIdata_complete/MRI_data_completeness.csv")
# subjects=df[df["DICOMS"]>0]["LI"]

df=pd.read_excel("/data/pt_02571/Data/01_sample/Samples_LIFE.xlsx")
subjects=df[(df["DICOM"]>0) & (df["Sample"] == "LC1")]["LI_Nr"]

for index, subject in enumerate(subjects):
    cmd = "python3 run_workflow_LC1.py s {}".format(subject)
    #my_file = "{}{}-test.sh".format(slurm_dir, subject)
    my_file = "{}/{}.sh".format(slurm_dir,subject)
    with open(my_file, "w") as my_file:
        print("#!/bin/bash\n#", file = my_file)
        print("#SBATCH -n 4", file = my_file)
        print("#SBATCH -N 1", file = my_file)
        print("#SBATCH --mem={}".format(mem_req), file = my_file)
        print("#SBATCH --time={}".format(tim_req), file = my_file)
        print("#SBATCH --job-name LCprep_{}".format(subject), file = my_file)
        print("#SBATCH --mail-type=begin", file = my_file)
        print("#SBATCH --mail-type=end", file = my_file)
        print("#SBATCH --mail-type=fail", file = my_file)
        print("#SBATCH --mail-user={}@cbs.mpg.de".format(user), file = my_file)
        print("#SBATCH -o {}/{}.out".format(slurm_dir,subject), file = my_file) 
        print("#SBATCH -e {}/{}.err\n".format(slurm_dir,subject), file = my_file)
        #write workflow command to sbatch file
        print("{}".format(cmd), file = my_file)
        
       
    with open("exec.sh", "w") as exec_file:
        print("chmod +x {}/{}.sh".format(slurm_dir,subject), file = exec_file)
        print("sbatch {}/{}.sh".format(slurm_dir,subject), file = exec_file)
        
    os.system("sh exec.sh")

        
