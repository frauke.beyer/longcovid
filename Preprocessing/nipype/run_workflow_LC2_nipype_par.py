import sys
sys.path.append('/data/pt_life/data_fbeyer/local2/miniconda2/envs/py392/site-packages/')
import os
from workflow import create_workflow
import pandas as pd
'''
Meta script to run preprocessing of long covid LC2
---------------------------------------------------
Can run in two modes:
python run_workflow.py s {subject_id}
python run_workflow.py f {text file containing list of subjects}
'''


df=pd.read_csv("/data/pt_02571/Data/A1_LC2/Sample_LCII_for_preproc.csv")

subjects=df[df["DICOM"]>0]["Sample of A1-LC2"]
#subjects=['LI08781694', 'LI08780670']


working_dir = '/data/pt_02571/Data/A1_LC2/wd/'

data_dir = '/data/pt_02571/Data/preprocessed/nipype/' 
input_dir= '/data/p_02571/mri/LC2/'
bids_dir = '/data/pt_02571/Data/A1_LC2/BIDS/'
freesurfer_dir = '/data/pt_02571/Data/preprocessed/freesurfer/' #  #' # ##
standard_brain = '/afs/cbs.mpg.de/software/fsl/6.0.3/debian-bullseye-amd64/data/standard/MNI152_T1_1mm.nii.gz' #for registration to MNI


create_workflow(subjects=subjects, 
            	input_dir=input_dir,
                bids_dir=bids_dir,
                working_dir=working_dir, 
                data_dir=data_dir,
                freesurfer_dir=freesurfer_dir,
                standard_brain=standard_brain)
