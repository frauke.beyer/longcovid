import nipype.interfaces.io as nio           # Data i/o
import nipype.interfaces.fsl as fsl
import nipype.interfaces.utility as util     # utility
import nipype.pipeline.engine as pe          # pypeline engine
from nipype import Function
from nipype.interfaces.dcm2nii import Dcm2niix #using Dcm2nixx for BIDS conversion
from nipype.interfaces.freesurfer import ReconAll
from bids.bids_interface import BidsConvert #import bids conversion interface
from util import change_sub
from structural.structural import create_structural

from diffusion.diffusion import create_dti


def create_workflow(subjects, input_dir, bids_dir, working_dir, data_dir, freesurfer_dir, standard_brain, ses):
    
    # set fsl output type to nii.gz
    fsl.FSLCommand.set_default_output_type('NIFTI_GZ')
    # main workflow
    preproc = pe.Workflow(name='preproc_lc1')
    preproc.base_dir = working_dir
    preproc.config['execution']['crashdump_dir'] = preproc.base_dir + "/crash_files"


    #Nodes
    subs_node=pe.Node(
                    name="subs_node",
                    interface=util.IdentityInterface(
                            fields=["subject"]))
    subs_node.iterables = ("subject", subjects)
    
    #select dicoms
    dicom_grabber = pe.Node(
                    name = "dicom_source_1",
                    interface = nio.DataGrabber(
                            infields = ["subject"],
                            outfields = ["dicom"],))
    dicom_grabber.inputs.sort_filelist = True
    dicom_grabber.inputs.base_directory = input_dir
    dicom_grabber.inputs.template = '%s_202*/DICOM/*'

    #adapt names of freesurfer & dwi to baseline/followup
    rename=pe.Node(name="rename", interface=Function(
                           input_names=["subj", "ses"],
                           output_names=["subj_f_preproc"],
                           function=change_sub))
    rename.inputs.ses=ses
    
    #convert dicoms
    dicom_convert = pe.Node(name="dicom_convert", interface=Dcm2niix())
    dicom_convert.inputs.out_filename="%p_s%s"
    
    #get info on dicoms
    from dicominfo import DicomInfo
    dicom_info = pe.Node(name="dicom_info", interface=DicomInfo())
    
    #bids converter
    bids_converter = pe.Node(name="bids_converter", interface=BidsConvert())

    bids_converter.inputs.bids_outputdir=bids_dir
    bids_converter.inputs.ses=ses
    
    #define structural and dwi workflows
    structural_wf = create_structural()
    structural_wf.inputs.inputnode.freesurfer_dir=freesurfer_dir
    structural_wf.inputs.inputnode.standard_brain=standard_brain
    
    dwi_wf = create_dti()
    dwi_wf.inputs.inputnode.freesurfer_dir=freesurfer_dir

    
    #data sink
    data_sink = pe.Node(name="data_sink", interface=nio.DataSink())
    data_sink.inputs.substitutions = [('_subject_', '')]
    data_sink.inputs.base_directory = data_dir

    preproc.connect([
        (subs_node, dicom_grabber, [("subject", "subject")]),
        (subs_node, bids_converter, [("subject", "subj")]),
        (dicom_grabber, dicom_convert, [("dicom", "source_names")]),
        (dicom_grabber, dicom_info, [("dicom", "files")]),
        (dicom_convert, bids_converter, [("bvals", "bvals")]),
        (dicom_convert, bids_converter, [("bvecs", "bvecs")]),
        (dicom_convert, bids_converter, [('bids', 'bids_info')]),
        (dicom_convert, bids_converter, [('converted_files', 'nii_files')]),
        (dicom_info, bids_converter, [('info', 'dicom_info')]),


        #structural workflow
        (subs_node, rename, [("subject", "subj")]),
        (rename, structural_wf, [("subj_f_preproc", "inputnode.subject")]),
        (bids_converter, structural_wf, [("T1", "inputnode.anat")]),          
        (structural_wf, data_sink, [('outputnode.brain', 'structural.@brain')]),
        (structural_wf, data_sink, [('outputnode.anat_head', 'structural.@anat_head')]),
        (structural_wf, data_sink, [('outputnode.brainmask', 'structural.@brainmask')]),
        (structural_wf, data_sink, [('outputnode.anat2std', 'structural.@anat2std')]),
        (structural_wf, data_sink, [('outputnode.anat2std_transforms', 'structural.@anat2std_transforms')]),
        (structural_wf, data_sink, [('outputnode.std2anat_transforms', 'structural.@std2anat_transforms')]),
        
        #diffusion workflow

        (structural_wf, dwi_wf, [("outputnode.subject_id", "inputnode.subject_id")]),
        (bids_converter, dwi_wf, [("dwi", "inputnode.dwi")]),
        (bids_converter, dwi_wf, [("dwi_ap", "inputnode.dwi_ap")]),
        (bids_converter, dwi_wf, [("dwi_pa", "inputnode.dwi_pa")]),
        (dicom_convert, dwi_wf, [("bvals", "inputnode.bvals")]),
        (dicom_convert, dwi_wf, [("bvecs", "inputnode.bvecs")]),

        (dicom_convert, data_sink, [("bvals", "diffusion.@bval")]),
        (dicom_convert, data_sink, [("bvecs", "diffusion.@bvecs")]),
        (dwi_wf, data_sink, [('outputnode.dwi_denoised', 'diffusion.@dwi_denoised')]),
        (dwi_wf, data_sink, [('outputnode.dwi_unringed', 'diffusion.@dwi_unringed')]),
        (dwi_wf, data_sink, [('outputnode.bo_brainmask', 'diffusion.@b0_mask')]),
        (dwi_wf, data_sink, [('outputnode.topup_corr', 'diffusion.@topup_corr')]),
        (dwi_wf, data_sink, [('outputnode.topup_field', 'diffusion.@topup_field')]),
        (dwi_wf, data_sink, [('outputnode.topup_fieldcoef', 'diffusion.@topup_fieldcoef')]),
        (dwi_wf, data_sink, [('outputnode.rotated_bvecs', 'diffusion.@rotated_bvecs')]),
        (dwi_wf, data_sink, [('outputnode.total_movement_rms', 'diffusion.@total_movement_rms')]),
        (dwi_wf, data_sink, [('outputnode.outlier_report', 'diffusion.@outlier_report')]),
	(dwi_wf, data_sink, [('outputnode.eddy_corr', 'diffusion.@eddy_corr')]),
        (dwi_wf, data_sink, [('outputnode.eddy_params', 'diffusion.@eddy_params')]),
        (dwi_wf, data_sink, [('outputnode.shell_alignment_parameters', 'diffusion.@shell_alignment_parameters')]),
        (dwi_wf, data_sink, [('outputnode.cnr_maps', 'diffusion.@cnr_maps')]),
        (dwi_wf, data_sink, [('outputnode.residuals', 'diffusion.@residuals')]),
        (dwi_wf, data_sink, [('outputnode.dti_fa', 'diffusion.@dti_fa')]),
        (dwi_wf, data_sink, [('outputnode.dti_md', 'diffusion.@dti_md')]),
        (dwi_wf, data_sink, [('outputnode.dti_l1', 'diffusion.@dti_l1')]),
        (dwi_wf, data_sink, [('outputnode.dti_l2', 'diffusion.@dti_l2')]),
        (dwi_wf, data_sink, [('outputnode.dti_l3', 'diffusion.@dti_l3')]),
        (dwi_wf, data_sink, [('outputnode.dti_v1', 'diffusion.@dti_v1')]),
        (dwi_wf, data_sink, [('outputnode.dti_v2', 'diffusion.@dti_v2')]),
        (dwi_wf, data_sink, [('outputnode.dti_v3', 'diffusion.@dti_v3')]),
        (dwi_wf, data_sink, [('outputnode.fa2anat', 'diffusion.@fa2anat')]),
        (dwi_wf, data_sink, [('outputnode.fa2anat_dat', 'diffusion.@fa2anat_dat')]),
        (dwi_wf, data_sink, [('outputnode.fa2anat_mat', 'diffusion.@fa2anat_mat')]),
        (dwi_wf, data_sink, [('outputnode.FW_DWI', 'diffusion.@FW_DWI')]),
        (dwi_wf, data_sink, [('outputnode.FW_FA0', 'diffusion.@FW_FA0')]),
        (dwi_wf, data_sink, [('outputnode.FW_FA', 'diffusion.@FW_FA')]),
        (dwi_wf, data_sink, [('outputnode.FW_MD', 'diffusion.@FW_MD')])
        ])
        
    preproc.run(plugin='MultiProc')#
        
