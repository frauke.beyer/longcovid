# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 11:08:45 2019

@author: fbeyer
"""

def create_bids(nii_files, dicom_info, bids_info, bids_outputdir, subj, bvals, bvecs, ses='01'):
    import os
    import shutil
    import re
    import json

    rs_acq=None
    
    c_appa=0

    bids_outputs=[]


    #print dicom_info
    if not (os.path.isdir('%s/sub-%s/ses-%s' %(bids_outputdir,subj,ses))):
        os.makedirs('%s/sub-%s/ses-%s' %(bids_outputdir,subj,ses))

    #create necessary folders.
    if not (os.path.isdir('%s/sub-%s/ses-%s/anat' %(bids_outputdir,subj,ses))):
        os.mkdir('%s/sub-%s/ses-%s/anat' %(bids_outputdir,subj,ses))
    if not (os.path.isdir('%s/sub-%s/ses-%s/func' %(bids_outputdir,subj,ses))):
        os.mkdir('%s/sub-%s/ses-%s/func' %(bids_outputdir,subj,ses))
    if not (os.path.isdir('%s/sub-%s/ses-%s/dwi' %(bids_outputdir,subj,ses))):
        os.mkdir('%s/sub-%s/ses-%s/dwi' %(bids_outputdir,subj,ses))
    if not (os.path.isdir('%s/sub-%s/ses-%s/fmap' %(bids_outputdir,subj,ses))):
        os.mkdir('%s/sub-%s/ses-%s/fmap' %(bids_outputdir,subj,ses))

    for sm in dicom_info:

        if sm['protocol_name']== None:
            continue
        #################### anatomical imaging ################################
        if sm['protocol_name']=="t2_spc_da-fl_irprep_sag_p2_iso" or sm['protocol_name']=="t2_spc_da-fl_irprep_p2_iso":
            #find list element from json
            r = re.compile('.*%s_s%s.json' %(sm['protocol_name'],sm['series_num']))
            sel_json=list(filter(r.match,bids_info))
            
            n = re.compile('.*%s_s%s.nii.gz' %(sm['protocol_name'],sm['series_num']))
            matched_nifti=list(filter(n.match, nii_files))
            
            print(matched_nifti[0])

            shutil.copyfile(matched_nifti[0], '%s/sub-%s/ses-%s/anat/sub-%s_ses-%s_FLAIR.nii.gz' %(bids_outputdir,subj,ses,subj,ses))
            shutil.copyfile(sel_json[0], '%s/sub-%s/ses-%s/anat/sub-%s_ses-%s_FLAIR.json' %(bids_outputdir,subj,ses,subj,ses))

            bids_outputs.append("FLAIR")


        elif sm['protocol_name']=="MPRAGE_ADNI_32Ch_PAT2":
            #find list element from json
            r = re.compile('.*%s_s%s.json' %(sm['protocol_name'],sm['series_num']))
            sel_json=list(filter(r.match,bids_info))
            
            n = re.compile('.*%s_s%s.nii.gz' %(sm['protocol_name'],sm['series_num']))
            matched_nifti=list(filter(n.match, nii_files))

            shutil.copyfile(matched_nifti[0], '%s/sub-%s/ses-%s/anat/sub-%s_ses-%s_acq-ADNI32ChPAT2_T1w.nii.gz' %(bids_outputdir,subj,ses,subj,ses))
            shutil.copyfile(sel_json[0], '%s/sub-%s/ses-%s/anat/sub-%s_ses-%s_acq-ADNI32ChPAT2_T1w.json' %(bids_outputdir,subj,ses,subj,ses))
            
            T1=matched_nifti[0]
            bids_outputs.append("MPRAGE_ADNI_32Ch_PAT2")

        #################### ap/pa for resting-state ################################
        elif sm['series_desc']=="cmrr_mbep2d_se":
            r = re.compile('.*%s_s%s.*.json' %(sm['protocol_name'],sm['series_num']))
            sel_json=list(filter(r.match,bids_info))

            n = re.compile('.*%s_s%s.nii.gz' %(sm['protocol_name'],sm['series_num']))
            matched_nifti=list(filter(n.match, nii_files))

            shutil.copyfile(matched_nifti[0], '%s/sub-%s/ses-%s/fmap/sub-%s_ses-%s_acq-rs_dir-norm_epi.nii.gz' %(bids_outputdir,subj,ses,subj,ses))
            shutil.copyfile(sel_json[0], '%s/sub-%s/ses-%s/fmap/sub-%s_ses-%s_acq-rs_dir-norm_epi.json' %(bids_outputdir,subj,ses,subj,ses))

            fname='%s/sub-%s/ses-%s/fmap/sub-%s_ses-%s_acq-rs_dir-norm_epi.json' %(bids_outputdir,subj,ses,subj,ses)

            with open(fname, 'r') as f:
                data = json.load(f)
                #print data
                data['IntendedFor']='ses-%s/func/sub-%s_ses-%s_task-rest_acq-cmrr_bold' %(ses,subj,ses)

            f.close()
            with open(fname, 'w') as f:
                json.dump(data, f)

            bids_outputs.append("cmrr_mbep2d_se_norm")

        elif sm['series_desc']=="cmrr_mbep2d_se_invpol" and c_appa==1: #use variable to indicate first and second appearance of this sequence

                r = re.compile('.*%s_s%s.*.json' %(sm['protocol_name'],sm['series_num']))
                sel_json=list(filter(r.match,bids_info))

                n = re.compile('.*%s_s%s.nii.gz' %(sm['protocol_name'],sm['series_num']))
                matched_nifti=list(filter(n.match, nii_files))

                shutil.copyfile(matched_nifti[0], '%s/sub-%s/ses-%s/fmap/sub-%s_ses-%s_acq-rs_dir-invpol_epi.nii.gz' %(bids_outputdir,subj,ses,subj,ses))
                shutil.copyfile(sel_json[0], '%s/sub-%s/ses-%s/fmap/sub-%s_ses-%s_acq-rs_dir-invpol_epi.json' %(bids_outputdir,subj,ses,subj,ses))

                fname='%s/sub-%s/ses-%s/fmap/sub-%s_ses-%s_acq-rs_dir-invpol_epi.json' %(bids_outputdir,subj,ses,subj,ses)
                with open(fname, 'r') as f:
                    data = json.load(f)
                    #print data
                    data['IntendedFor']='ses-%s/func/sub-%s_ses-%s_task-rest_acq-cmrr_bold.nii.gz' %(ses,subj,ses)

                f.close()
                with open(fname, 'w') as f:
                    json.dump(data, f)

                bids_outputs.append("cmrr_mbep2d_se_invpol")
                

        #################### field map for resting-state ################################
        elif sm['series_desc']=="gre_field_mapping" and sm['image_type'][2]=='M':

            print("gre_field_mapping")
            #find list element from json
            r = re.compile('.*%s_s%s.*.json' %(sm['protocol_name'],sm['series_num']))
            sel_json=list(filter(r.match,bids_info))
            
            n = re.compile('.*%s_s%s_e1.nii.gz' %(sm['protocol_name'],sm['series_num']))
            matched_nifti=list(filter(n.match, nii_files))

            shutil.copyfile(matched_nifti[0],'%s/sub-%s/ses-%s/fmap/sub-%s_ses-%s_acq-rs_magnitude1.nii.gz' %(bids_outputdir,subj,ses,subj,ses))
            fname='%s/sub-%s/ses-%s/fmap/sub-%s_ses-%s_acq-rs_phasediff.json' %(bids_outputdir,subj,ses,subj,ses)
            shutil.copyfile(sel_json[0], fname)

            with open(fname, 'r') as f:
                data = json.load(f)
                if 'echo_times' in sm:
                    data['EchoTime1']=sm['echo_times'][0]*0.001
                    data['EchoTime2']=sm['echo_times'][1]*0.001
                elif 'echo_time' in sm:
                    print("has only one echo time (older version of scanner software maybe)")
                    data['EchoTime1']=(sm['echo_time']-2.46)*0.001
                    data['EchoTime2']=sm['echo_time']*0.001
                else:
                    "no echo time found for this fieldmap"
                if isinstance(rs_acq, str):
                    data["IntendedFor"] = rs_acq
                f.close()
            with open(fname, 'w') as f:
                json.dump(data, f)

            bids_outputs.append("gre_field_mapping")
        elif sm['series_desc']=="gre_field_mapping" and sm['image_type'][2]=='P':
            #find list element from json
            r = re.compile('.*%s_s%s.*.json' %(sm['protocol_name'],sm['series_num']))
            sel_json=list(filter(r.match,bids_info))


            n = re.compile('.*%s_s%s.*_ph.nii.gz' %(sm['protocol_name'],sm['series_num']))
            matched_nifti=list(filter(n.match, nii_files))

            shutil.copyfile(matched_nifti[0],'%s/sub-%s/ses-%s/fmap/sub-%s_ses-%s_acq-rs_phasediff.nii.gz' %(bids_outputdir,subj,ses,subj,ses))

        #################### resting-state ################################
        elif sm['series_desc']=="cmrr_mbep2d_resting":
            
            

            r = re.compile('.*%s_s%s.json' %(sm['protocol_name'],sm['series_num']))
            sel_json=list(filter(r.match,bids_info))

            n = re.compile('.*%s_s%s.nii.gz' %(sm['protocol_name'],sm['series_num']))
            matched_nifti=list(filter(n.match, nii_files))

            shutil.copyfile(matched_nifti[0], '%s/sub-%s/ses-%s/func/sub-%s_ses-%s_task-rest_acq-cmrr_bold.nii.gz' %(bids_outputdir,subj,ses,subj,ses))
            shutil.copyfile(sel_json[0], '%s/sub-%s/ses-%s/func/sub-%s_ses-%s_task-rest_acq-cmrr_bold.json' %(bids_outputdir,subj,ses,subj,ses))

            fname='%s/sub-%s/ses-%s/func/sub-%s_ses-%s_task-rest_acq-cmrr_bold.json' %(bids_outputdir,subj,ses,subj,ses)
            with open(fname, 'r') as f:
                data = json.load(f)
                #print data
                data['TaskName']='rest'

            f.close()
            with open(fname, 'w') as f:
                json.dump(data, f)

            rs_acq ='ses-%s/func/sub-%s_ses-%s_task-rest_acq-cmrr_bold.nii.gz' %(ses,subj,ses)
            fname = '%s/sub-%s/ses-%s/fmap/sub-%s_ses-%s_acq-rs_phasediff.json' % (bids_outputdir, subj, ses, subj, ses)
            if os.path.isfile(fname):
                with open(fname, 'r') as f:
                    data = json.load(f)
                    # print data
                    data['IntendedFor'] = rs_acq

                f.close()
                with open(fname, 'w') as f:
                    json.dump(data, f)

            bids_outputs.append("cmrr_mbep2d_resting")    
        
    
        #################### field map for diffusion ################################
        elif sm['series_desc']=="cmrr_mbep2d_se_norm":
            r = re.compile('.*%s_s%s.*.json' %(sm['protocol_name'],sm['series_num']))
            sel_json=list(filter(r.match,bids_info))

    
            n = re.compile('.*%s_s%s.nii.gz' %(sm['protocol_name'],sm['series_num']))
            matched_nifti=list(filter(n.match, nii_files))

            shutil.copyfile(matched_nifti[0], '%s/sub-%s/ses-%s/fmap/sub-%s_ses-%s_acq-dwi_dir-norm_epi.nii.gz' %(bids_outputdir,subj,ses,subj,ses))
            shutil.copyfile(sel_json[0], '%s/sub-%s/ses-%s/fmap/sub-%s_ses-%s_acq-dwi_dir-norm_epi.json' %(bids_outputdir,subj,ses,subj,ses))

            fname='%s/sub-%s/ses-%s/fmap/sub-%s_ses-%s_acq-dwi_dir-norm_epi.json' %(bids_outputdir,subj,ses,subj,ses)

            with open(fname, 'r') as f:
                data = json.load(f)
                #print data
                data['IntendedFor']='ses-%s/dwi/sub-%s_ses-%s_acq-cmrr_dwi.nii.gz' %(ses,subj,ses)

            f.close()
            with open(fname, 'w') as f:
                json.dump(data, f)

            dwi_ap=matched_nifti[0]
            bids_outputs.append("cmrr_mbep2d_se_norm")

        elif sm['series_desc']=="cmrr_mbep2d_se_invpol" and c_appa==0:

                r = re.compile('.*%s_s%s.*.json' %(sm['protocol_name'],sm['series_num']))
                sel_json=list(filter(r.match,bids_info))

                n = re.compile('.*%s_s%s.nii.gz' %(sm['protocol_name'],sm['series_num']))
                matched_nifti=list(filter(n.match, nii_files))

                shutil.copyfile(matched_nifti[0], '%s/sub-%s/ses-%s/fmap/sub-%s_ses-%s_acq-dwi_dir-invpol_epi.nii.gz' %(bids_outputdir,subj,ses,subj,ses))
                shutil.copyfile(sel_json[0], '%s/sub-%s/ses-%s/fmap/sub-%s_ses-%s_acq-dwi_dir-invpol_epi.json' %(bids_outputdir,subj,ses,subj,ses))

                fname='%s/sub-%s/ses-%s/fmap/sub-%s_ses-%s_acq-dwi_dir-invpol_epi.json' %(bids_outputdir,subj,ses,subj,ses)
                with open(fname, 'r') as f:
                    data = json.load(f)
                    #print data
                    data['IntendedFor']='ses-%s/dwi/sub-%s_ses-%s_acq-cmrr_dwi.nii.gz' %(ses,subj,ses)

                f.close()
                with open(fname, 'w') as f:
                    json.dump(data, f)
                    
                dwi_pa=matched_nifti[0]
                c_appa=1 #DWI ap-pa comes before resting state

        ################ diffusion-weighted imaging ################
        elif sm['series_desc']== "cmrr_mbep2d_diff":

            #find list element from json
            r = re.compile('.*%s_s%s.json' %(sm['protocol_name'],sm['series_num']))
            sel_json=list(filter(r.match,bids_info))

            n = re.compile('.*%s_s%s.nii.gz' %(sm['protocol_name'],sm['series_num']))
            matched_nifti=list(filter(n.match, nii_files))

            shutil.copyfile(matched_nifti[0], '%s/sub-%s/ses-%s/dwi/sub-%s_ses-%s_acq-cmrr_dwi.nii.gz' %(bids_outputdir,subj,ses,subj,ses))
            shutil.copyfile(sel_json[0], '%s/sub-%s/ses-%s/dwi/sub-%s_ses-%s_acq-cmrr_dwi.json' %(bids_outputdir,subj,ses,subj,ses))
            
            
            #copy bval/bvec files coming from dcm2niix

            shutil.copyfile(bvecs[0], '%s/sub-%s/ses-%s/dwi/sub-%s_ses-%s_acq-cmrr_dwi.bvec' %(bids_outputdir,subj,ses,subj,ses))
            shutil.copyfile(bvals[0], '%s/sub-%s/ses-%s/dwi/sub-%s_ses-%s_acq-cmrr_dwi.bval' %(bids_outputdir,subj,ses,subj,ses))

            dwi='%s/sub-%s/ses-%s/dwi/sub-%s_ses-%s_acq-cmrr_dwi.nii.gz' %(bids_outputdir,subj,ses,subj,ses)
            bids_outputs.append("cmrr_mbep2d_diff")

        
        ################ SWI imaging ################
        elif sm['protocol_name']=="as_gre_TE17ms_nifti":
            if sm['image_type'][2]=='M':
                
                #find list element from json
                r = re.compile('.*%s_s%s.json' %(sm['protocol_name'],sm['series_num']))
                sel_json=list(filter(r.match,bids_info))
                
                n = re.compile('.*%s_s%s.nii.gz' %(sm['protocol_name'],sm['series_num']))
                matched_nifti=list(filter(n.match, nii_files))

                shutil.copyfile(matched_nifti[0], '%s/sub-%s/ses-%s/anat/sub-%s_ses-%s_acq-mag_swi.nii.gz' %(bids_outputdir,subj,ses,subj,ses))
                shutil.copyfile(sel_json[0], '%s/sub-%s/ses-%s/anat/sub-%s_ses-%s_acq-mag_swi.json' %(bids_outputdir,subj,ses,subj,ses))

            elif sm['image_type'][2]=='P':
                #find list element from json
                r = re.compile('.*%s_s%s_ph.json' %(sm['protocol_name'],sm['series_num']))

                n = re.compile('.*%s_s%s_ph.nii.gz' %(sm['protocol_name'],sm['series_num']))
                matched_nifti=list(filter(n.match, nii_files))
                print(matched_nifti)
    
                shutil.copyfile(matched_nifti[0], '%s/sub-%s/ses-%s/anat/sub-%s_ses-%s_acq-ph_swi.nii.gz' %(bids_outputdir,subj,ses,subj,ses))
                shutil.copyfile(sel_json[0], '%s/sub-%s/ses-%s/anat/sub-%s_ses-%s_acq-ph_swi.json' %(bids_outputdir,subj,ses,subj,ses))


        else:
                print("Series %s of Protocol %s could not be matched" %(sm['series_desc'], sm["protocol_name"]))

    return bids_outputs, T1, dwi_ap, dwi_pa, dwi
