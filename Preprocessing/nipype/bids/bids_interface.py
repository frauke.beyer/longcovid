import os
import sys
import math

from nipype.interfaces.base.core import BaseInterface
from nipype.interfaces.base.specs import TraitedSpec
from traits.api import Dict, List, String, Either
from nipype.interfaces.base.traits_extension import BasePath, Directory, File, InputMultiObject, InputMultiPath
from traits.trait_errors import TraitError
import nipype.interfaces.dcm2nii as d2n
from nipype.utils.filemanip import split_filename
import numpy as np

class BidsConvertInputSpec(TraitedSpec):
    
    nii_files = InputMultiPath(
            mandatory=True,
            desc="List of nii files (str) corresponding to each session in the scan.")
    bids_info = InputMultiPath(
            mandatory=True,
            desc="List of json files (str) corresponding to each session in the scan.")
    bvecs=InputMultiPath(
            mandatory=True,
            desc="List of bvecs (str) corresponding to each session in the scan.")
    bvals=InputMultiPath(
            mandatory=True,
            desc="List of bvals (str) corresponding to each session in the scan.")
    dicom_info = List(
            mandatory=True,
            desc="one dict for each series in the session, in the order they were\
                  run. each dict should contain at least the series_num (int), \
                  the series_desc (str). and the corresponding nifti (str)")
    bids_outputdir = Directory(
            mandatory=True,
            desc="a directory used for bids_output")
    subj = String(
            mandatory=True,
            desc="a subject and time of scan identifier")
    ses = String(
            desc="which MRI session: 01 is baseline, 02 is followup")

class BidsConvertOutputSpec(TraitedSpec):
    bids_outputs =InputMultiPath(desc="an list of files saved in bids format")
    T1 = File(desc="path to T1 file")
    dwi_ap = File(desc="path to ap file")
    dwi_pa = File(desc="path to pa file")
    dwi= File(desc="path to dwi file")

class BidsConvert(BaseInterface):
    input_spec = BidsConvertInputSpec
    output_spec = BidsConvertOutputSpec

    def __init__(self, *args, **kwargs):
        super(BidsConvert, self).__init__(*args, **kwargs)
        self.bids_outputs = []

    def _run_interface(self, runtime):
        from bids.bids_conversion import create_bids
        self.bids_outputs = []
        bids_outputdir = self.inputs.bids_outputdir
        subj = self.inputs.subj
        nii_files = self.inputs.nii_files
        dicom_info = self.inputs.dicom_info
        bids_info = self.inputs.bids_info
        bvecs=self.inputs.bvecs
        bvals=self.inputs.bvals
        ses=self.inputs.ses


        [bids_outputs,T1, dwi_ap, dwi_pa, dwi ]=create_bids(nii_files, dicom_info, bids_info, bids_outputdir, subj, bvals, bvecs, ses)
        self.bids_outputs=bids_outputs
        self.T1 = T1
        self.dwi_ap = dwi_ap
        self.dwi_pa = dwi_pa
        self.dwi = dwi

        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs["bids_outputs"] = self.bids_outputs
        outputs["T1"] = self.T1
        outputs["dwi_ap"] = self.dwi_ap
        outputs["dwi_pa"] = self.dwi_pa
        outputs["dwi"] = self.dwi

        return outputs
