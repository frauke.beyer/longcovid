import os
import sys
import math

from nipype.interfaces.base.core import BaseInterface
from nipype.interfaces.base.specs import TraitedSpec
from traits.api import Dict, List, String, Either
from nipype.interfaces.base.traits_extension import BasePath, Directory, File, InputMultiObject, InputMultiPath
from traits.trait_errors import TraitError
import nipype.interfaces.dcm2nii as d2n
from nipype.utils.filemanip import split_filename
import numpy as np

class FwCorrectionInputSpec(TraitedSpec):
      
    preprocessedDWI= String(
            mandatory=True,
            desc="Path to eddy preprocessed DWI.")
    bvecs=String(
            mandatory=True,
            desc="Path to bvecs.")
    bvals=String(
            mandatory=True,
            desc="Path to bvals")
    brainmaskDWI = String(
            mandatory=True,
            desc="Path to brainmask")

class FwCorrectionOutputSpec(TraitedSpec):

    FW_DWI = String(desc="path to free-water corrected DWI file")
    FW_FA0 = String(desc="path to raw FA file")
    FW_FA = String(desc="path to free-water corrected FA file")
    FW_MD = String(desc="path to free-water corrected MD file")



class FwCorrection(BaseInterface):
    input_spec = FwCorrectionInputSpec
    output_spec = FwCorrectionOutputSpec

    def __init__(self, *args, **kwargs):
        super(FwCorrection, self).__init__(*args, **kwargs)
        self.outputs = []

    def _run_interface(self, runtime):
        from diffusion.fw_mrn import run_freewater_correction
        self.outputs = []
        brainmaskDWI = self.inputs.brainmaskDWI
        preprocessedDWI = self.inputs.preprocessedDWI
        bvecs=self.inputs.bvecs
        bvals=self.inputs.bvals


        [FW_DWI, FW_FA0, FW_FA, FW_MD ]=run_freewater_correction(preprocessedDWI, brainmaskDWI, bvals, bvecs)
        self.FW_DWI=FW_DWI
        self.FW_FA0 =FW_FA0
        self.FW_FA =FW_FA
        self.FW_MD= FW_MD



        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs["FW_DWI"] = self.FW_DWI
        outputs["FW_FA0"] = self.FW_FA0
        outputs["FW_FA"] = self.FW_FA
        outputs["FW_MD"] = self.FW_MD


        

        return outputs
