import sys
sys.path.append('/data/pt_life/data_fbeyer/local2/miniconda2/envs/py392/site-packages/')
import os
from workflow import create_workflow
import pandas as pd

'''
Meta script to run preprocessing of long covid LC1
---------------------------------------------------
Can run in two modes:
python run_workflow.py s {subject_id}
python run_workflow.py f {text file containing list of subjects}
'''


df=pd.read_csv("/data/pt_02571/Data/A1_LC1/QualityCheck_MRIdata_complete/MRI_data_completeness.csv")
subjects=df[df["DICOMS"]>0]["LI"]
#


working_dir = '/data/pt_02571/Data/A1_LC1/wd/'
data_dir = '/data/pt_02571/Data/preprocessed/nipype/' 
input_dir= "/data/p_02571/mri/LC1/Subjects/"
bids_dir = '/data/p_02571/mri/LC1/BIDS/'
freesurfer_dir = '/data/pt_02571/Data/preprocessed/freesurfer/' #  #' # ##
standard_brain = '/afs/cbs.mpg.de/software/fsl/6.0.3/debian-bullseye-amd64/data/standard/MNI152_T1_1mm.nii.gz' #for registration to MNI


create_workflow(subjects=subjects, 
            	input_dir=input_dir,
                bids_dir=bids_dir,
                working_dir=working_dir, 
                data_dir=data_dir,
                freesurfer_dir=freesurfer_dir,
                standard_brain=standard_brain)
