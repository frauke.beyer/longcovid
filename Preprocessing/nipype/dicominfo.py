from nipype.interfaces.base.core import BaseInterface
from nipype.interfaces.base.specs import TraitedSpec
from traits.api import Dict
from nipype.interfaces.base.traits_extension import BasePath, Directory, File, InputMultiObject, InputMultiPath
from util import orientation_from_dcm_header

class DicomInfoInputSpec(TraitedSpec):
    files = InputMultiPath(File,
            desc="a list of dicom files from which to extract data",
            copyfile=False
            )

class DicomInfoOutputSpec(TraitedSpec):
    info = InputMultiPath(Dict(),
            desc="an ordered list of dicts, all in the same directory.")

class DicomInfo(BaseInterface):
    input_spec = DicomInfoInputSpec
    output_spec = DicomInfoOutputSpec

    def __init__(self, *args, **kwargs):
        super(DicomInfo, self).__init__(*args, **kwargs)
        self.info = []

    def _run_interface(self, runtime):
        import pydicom
        files = self.inputs.files
        by_series = {}
        self.info = []
        for f in files:
            d = pydicom.dcmread(f)
            try: 
                s_num = d.SeriesNumber
                s_num = int(s_num)
            except Exception as e:
                raise e
            
            if not s_num in by_series:
                by_series[s_num] = {
                    "series_num": s_num,
                    "series_desc": getattr(d,"SeriesDescription",None),
                    "protocol_name": getattr(d,"ProtocolName",None),
                }
                if [0x19,0x1018] in d and \
                        "description" in dir(d[0x19,0x1018]) and \
                        "RealDwellTime" in d[0x19,0x1018].description():
                    try:
                        by_series[s_num]["RealDwellTime"] = float(d[0x19,0x1018].value)
                    except Exception as e:
                        pass # don't die
                it = getattr(d,"ImageType",None)
                if it:
                    if not isinstance(it,str):
                        it = list(it)
   
                    by_series[s_num]["image_type"] = it
                    
                ipped = getattr(d,"InPlanePhaseEncodingDirection", None)
                if ipped:
                    by_series[s_num]["ipp_encoding_direction"] = ipped
                
                # ep repetition time
                rep = d.get((0x0018,0x0080), None)
                if rep:
                    try:
                        rep = float(rep.value)/1000 #to get it in seconds
                    except Exception as e:
                        pass
                    else:
                        by_series[s_num]["TR"] = rep 
                        
                # ep echo spacing ingredients
                bpppe = d.get((0x0019,0x1028), None)
                if bpppe:
                    try:
                        bpppe = float(bpppe.value)
                    except Exception as e:
                        pass
                    else:
                        by_series[s_num]["bw_per_pix_phase_encode"] = bpppe 
                        
                acq_mat = getattr(d, "AcquisitionMatrix", None)
                if acq_mat and len(acq_mat) == 4:
                    # acq mat text gets turned into this struct.
                    # n will be in 0/1 (other index will == 0), m will be in 2/3
                    by_series[s_num]["acq_matrix_n"] = acq_mat[0] or acq_mat[1]
                    by_series[s_num]["acq_matrix_m"] = acq_mat[2] or acq_mat[3]
                # try to get orientation from header
                orient = orientation_from_dcm_header(d)
                if orient:
                    by_series[s_num]["orientation"] = orient
                # try to get siemens shadow header
                #try:
                #    ss = read_siemens_shadow(f)[0]
                #except Exception, e:
                #    pass
                #else:
                #    siemens_keys = ["in_plane_rotation", "polarity_swap"]
                #    by_series[s_num].update(dict([(k,v) for k,v in ss.iteritems() if k in siemens_keys]))
            bs = by_series[s_num]
            # collect all of the unique EchoTimes per series
            try:
                et = getattr(d,"EchoTime",None)
                if et:
                    et = float(et)
                    if not "echo_times" in bs:
                        bs["echo_times"] = []
                    if not et in bs["echo_times"]:
                        bs["echo_times"].append(et)
            except Exception as e:
                pass # don't die.
        
        
        for s_num, s_info in by_series.items():
            # find delta_te, collapse to echo time if needed
            if "echo_times" in s_info:
                etc = len(s_info["echo_times"])
                if etc == 0:
                    del s_info["echo_times"]
                elif etc == 1:
                    s_info["echo_time"] = s_info["echo_times"][0]
                    del s_info["echo_times"]
                elif etc == 2:
                    s_info["delta_te"] = abs(s_info["echo_times"][0] - s_info["echo_times"][1])
        for k in sorted(by_series.keys()):
            self.info.append(by_series[k])
        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        outputs["info"] = self.info
        return outputs