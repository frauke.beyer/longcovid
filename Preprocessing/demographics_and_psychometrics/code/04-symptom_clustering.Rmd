# Symptom Clustering
Evans et al. 2021 showed that there are distinct clusters with respective to the severity of physical and mental health impairment and cognitive functioning. We aim to replicate / build on this finding.

## Simulate Data
Let's begin by testing various cluster-approaches using a simulated dataset.
There are multiple options to simulate clustered data:

* Various clustering packages are too restricted for our purposes: fungible::monte() (https://search.r-project.org/CRAN/refmans/fungible/html/monte.html), simStudy::genCluster()  (https://rdrr.io/cran/simstudy/man/genCluster.html), kamila::genMixedData() (https://rdrr.io/cran/kamila/man/genMixedData.html) and also fabricatr
* We could do it from scratch, maybe by having a look statistical rethinking for inspiration. But nonetheless especially tricky is simulating clustered data from both continual and nominal data
* my favorite approach to this point would be to use the SimMultiCorrData package (https://cran.r-project.org/web/packages/SimMultiCorrData/SimMultiCorrData.pdf#Rfn.rcorrvar), an seemingly impressive package to simulate real life data!

So what are we aiming for? Certainly we want to have one dataset which pretty much resembles our expectations (to test whether respective cluster algorithms will get us an expected output) and another dataset which contains no information (to see which algorithm best indicates null-results). (?)

To simulate with the SimMultiCorrData package, we first will replicate the workflow as mentioned in https://cran.r-project.org/web/packages/SimMultiCorrData/vignettes/workflow.html before simulating datapoints which is more closeley related to our prospective dataset

### Using SimMultiCorrData
Adapting the workflow from Allison Fialkowski we first define the properties of our variables of interest. 

#### Step 1: Set up the distributions and obtain the standardized cumulants {-}

```{r}
library("SimMultiCorrData")
library("printr")

# Turn off scientific notation
options(scipen = 999)

# Set seed and sample size
seed <- 11
n <- 50

# Continuous Distributions
Dist <- c("Gaussian", "Chisq", "Beta")

# Calculate standardized cumulants
# Those for the normal distribution are rounded to ensure the correct values 
# are obtained.
M1 <- round(calc_theory(Dist = "Gaussian", params = c(0, 1)))
M2 <- calc_theory(Dist = "Chisq", params = 4)
M3 <- calc_theory(Dist = "Beta", params = c(4, 2))
M <- cbind(M1, M2, M3)

# Binary and Ordinal Distributions
marginal <- list(c(0.3, 0.75), c(0.2, 0.5, 0.9))
support <- list() # default support will be generated inside simulation

# Poisson Distributions
lam <- c(1, 5, 10)

# Negative Binomial Distributions
size <- c(3, 6)
prob <- c(0.2, 0.8)

ncat <- length(marginal)
ncont <- ncol(M)
npois <- length(lam)
nnb <- length(size)

# Create correlation matrix from a uniform distribution (0.2, 0.7)
set.seed(seed)
Rey <- diag(1, nrow = (ncat + ncont + npois + nnb))
for (i in 1:nrow(Rey)) {
  for (j in 1:ncol(Rey)) {
    if (i > j) Rey[i, j] <- runif(1, 0.2, 0.7)
    Rey[j, i] <- Rey[i, j]
  }
}

# Check to see if Rey is positive-definite
min(eigen(Rey, symmetric = TRUE)$values) < 0
```

#### Step 2: Calculate the lower kurtosis bounds for the continuous variables {-}

```{r}
Lower <- list()

# list of standardized kurtosis values to add in case only invalid power 
#     method pdfs are produced
Skurt <- list(seq(0.5, 2, 0.5), seq(0.02, 0.05, 0.01), seq(0.02, 0.05, 0.01))

start.time <- Sys.time()
for (i in 1:ncol(M)) {
  Lower[[i]] <- calc_lower_skurt(method = "Polynomial", skews = M[3, i], 
                                 fifths = M[5, i], sixths = M[6, i], 
                                 Skurt = Skurt[[i]], seed = 104)
}
stop.time <- Sys.time()
Time <- round(difftime(stop.time, start.time, units = "min"), 3)
cat("Total computation time:", Time, "minutes \n")
```

In each case, the lower kurtosis boundary calculated from the original Lagrangean constraint equations (see poly_skurt_check) generates constants that yield an invalid power method pdf. This is indicated by the fact that each Invalid.C data.frame contains solutions (i.e. see Lower[[2]]$Invalid.C).

For Distributions 2 and 3, lower kurtosis values that generate constants that yield valid power method pdfs could be found by adding the values displayed in SkurtCorr1 to the original lower kurtosis boundary. For Distribution 1 (Normal), no kurtosis addition (of those specified in Skurt) generated constants that yield a valid pdf. This does not cause a problem since the simulated variable has a valid power method pdf.

Look at lower kurtosis boundaries and sixth cumulant corrections:

```{r}
as.matrix(Lower[[1]]$Min[1, c("skew", "fifth", "sixth", "valid.pdf", 
                              "skurtosis")], 
          nrow = 1, ncol = 5, byrow = TRUE) 
```
Note that valid.pdf = FALSE, which means that a kurtosis correction could not be found that yielded constants that produce a valid power method pdf. The original lower kurtosis boundary (see Lower[[1]]$Min) is -0.282974. The standardized kurtosis for the distribution (0) falls above this boundary.
```{r}
as.matrix(Lower[[2]]$Min[1, c("skew", "fifth", "sixth", "valid.pdf", 
                              "skurtosis")], 
          nrow = 1, ncol = 5, byrow = TRUE)

Lower[[2]]$SkurtCorr1
```

The original lower kurtosis boundary (see Lower[[2]]$Invalid.C) of 2.975959 has been increased to 3.005959, so that the kurtosis correction is 0.03. The standardized kurtosis for the distribution (3) is approximately equal to this boundary. This does not cause a problem since the simulated variable has a valid power method pdf.

```{r}
as.matrix(Lower[[3]]$Min[1, c("skew", "fifth", "sixth", "valid.pdf", 
                              "skurtosis")], 
          nrow = 1, ncol = 5, byrow = TRUE) 

Lower[[3]]$SkurtCorr1
```

The original lower kurtosis boundary (see Lower[[3]]$Invalid.C) of -0.392266 has been increased to -0.372266, so that the kurtosis correction is 0.02. The standardized kurtosis for the distribution (-0.2727) falls above this boundary.

The remaining steps vary by simulation method, we will concentrade for now on Method 1!

#### Step 3: Verify the target correlation matrix falls within the feasible correlation bounds {-}

```{r}
valid <- valid_corr(k_cat = ncat, k_cont = ncont, k_pois = npois,
                    k_nb = nnb, method = "Polynomial", means =  M[1, ],
                    vars =  (M[2, ])^2, skews = M[3, ], skurts = M[4, ],
                    fifths = M[5, ], sixths = M[6, ], marginal = marginal, 
                    lam = lam, size = size, prob = prob, rho = Rey, 
                    seed = seed)
```

#### Step 4: Generate the variables {-}
Simulate variables without the error loop.
```{r}
A <- rcorrvar(n = 50, k_cont = ncont, k_cat = ncat, k_pois = npois,
              k_nb = nnb, method = "Polynomial", means =  M[1, ], 
              vars =  (M[2, ])^2, skews = M[3, ], skurts = M[4, ], 
              fifths = M[5, ], sixths = M[6, ], marginal = marginal,
              lam = lam, size = size, prob = prob, rho = Rey, seed = seed)
```
#### Step 5: Checking results {-}
We will come back to this point.

#### Step 6: Applying clustering methods to dataset

First we will create the dataset of interest

```{r}
cluster_df <- as_tibble(cbind(A$ordinal_variables, A$continuous_variables), .name_repair = ~ c("ordinal_one", "ordinal_two", "continuous_one", "continuous_two", "continuous_three"))
```

Next, we will use PAM on the specified data.
```{r}
sim_ordinals <- c("ordinal_one", "ordinal_two")

cluster_df[sim_ordinals] <- lapply(cluster_df[sim_ordinals], factor)

dist_gower <- daisy(cluster_df, metric = "gower", stand = FALSE)
gower_matrix <- as.matrix(dist_gower)

DistanceMap <- fviz_dist(dist_gower, order = TRUE, show_labels = TRUE, lab_size = 4) + labs(title = "Distance: Sim Measurements")
fviz_nbclust(gower_matrix, pam, method ="silhouette")+theme_minimal()

metoids <- pam(gower_matrix,4, metric = "manhattan", stand = FALSE)
fviz_cluster(metoids, 
             #palette =c("#007892","#D9455F"),
             ellipse.type ="euclid",
             repel =TRUE,
             ggtheme =theme_minimal())
```



### Using predefined functions {-}
Let's start with trying the genMixedData() function from the kamila package
```{r}
sim_df <- genMixedData(sampSize = 50, nConVar = 2, nCatVar = 10, nCatLevels = 4, conErrLev = 0.4, nConWithErr=1, nCatWithErr=1, catErrLev = 0.3, popProportions=c(0.3,0.7))

sim_df
```

Seems to me way too restricted to be of any use. 

Next we will have a look at fabricatr. 
```{r}
categorical_example <- fabricate(
  N = 50,
  p1 = runif(N, 0, 1),
  p2 = runif(N, 0, 1),
  p3 = runif(N, 0, 1),
  cat = draw_categorical(N = N, prob = c(0.5, 0.5)))

# using draw_binary_iccc

## 100 individual population, 20 each in each of 5 clusters
clusters = rep(1:5, 20)

## Individuals have a 20% chance of smoking, but clusters are highly correlated
## in their tendency to smoke
smoker = draw_binary_icc(prob = 0.2, clusters = clusters, ICC = 0.5)

## health maniac
clusters <- c(rep(1:2, 50))
maniacs <- draw_binary_icc(prob = 0.5, clusters = clusters, ICC = 0.8)

## sim_iq
clusters <- c(rep(1:2, 50))
sim_iq <- draw_binary_icc(prob = 0.5, clusters = clusters, ICC = 0.8)

## noise
clusters <- c(rep(1:2, 50))
noise <- draw_binary_icc(prob = 0.5, clusters = clusters, ICC = 0.1)


cluster_df <- cbind(smoker, maniacs, sim_iq, noise)  
cluster_df <- as_tibble(cluster_df)


```


### Statistical Rethinking {-}
### Doing it from scratch {-}

## Metoids Clustering using PAM

There are multiple decisions to make (infos to get): 

* what input do we want to give PAM? (do we have some kind os dissimilarity matrix and if so, do we use the gower metric? Or euclidean metric? 
* Applying PAM we again have to decide if we want to use the manhattan or euclidean metric (manhattan metric has advantages in case there are outliers expected)).
* Do we try to copy the exact configurations as in Evans et al. (2021)?
```{r}
# Using the simulated data
sim_symptoms <- c("smoker", "maniacs", "sim_iq", "noise")
cluster_df[sim_symptoms] <- lapply(cluster_df[sim_symptoms], factor)

dist_gower <- daisy(cluster_df, metric = "gower", stand = FALSE)
gower_matrix <- as.matrix(dist_gower)

DistanceMap <- fviz_dist(dist_gower, order = TRUE, show_labels = TRUE, lab_size = 4) + labs(title = "Distance: Sim Measurements")
fviz_nbclust(gower_matrix, pam, method ="silhouette")+theme_minimal()

metoids <- pam(gower_matrix,2, metric = "manhattan", stand = FALSE)
fviz_cluster(metoids, 
             palette =c("#007892","#D9455F"),
             ellipse.type ="euclid",
             repel =TRUE,
             ggtheme =theme_minimal())


# Using the real data
temp_df <- LC_one %>% select(age, all_of(symptom_list)) %>%
  mutate_at("age", ~ scale(.) %>% as_vector %>% .[,1])

temp_df[symptom_list] <- lapply(temp_df[symptom_list], factor)
## to be continued..
```

## Latent variable (mixture) modeling
## HDBSCAN
A recent development in the field of density-based clustering methods is Hierarchical Density-Based Spatial Clustering of Applications with Noise (HDBSCAN) (for r implementation see https://rdrr.io/cran/dbscan/man/hdbscan.html, for further information see Campello et al., 2013). 
Density-based clustering methods are designed to identify clusters of varying densities. As such, these methods are particularly useful in separating smaller subclusters (with higher densities) from larger clusters (with lower densities).

## TRYOUT DAtA
```{r}
file_path <- "/data/pt_02571/Data/01_sample/test_data/cancer_patient_data_sets.xlsx"
data <- read_xlsx(file_path)
```

