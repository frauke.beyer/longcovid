## Vasculature Preprocessing
We begin with setting some standard directories
```{r standard dirs}
base_dir <- "/data/pt_02571/Data/preprocessed/qsm"
vasculature_dir <- "/data/pt_02571/Data/preprocessed/vasculature"
```


### clearswi
Make sure to follow the instructions as outlined in - install julia, set path to julia, add package and run the clearswi command with your specific TE etc. values
```{r clearswi}
qsm_dir <- sprintf("%s", grep(list.dirs(path = base_dir, recursive = FALSE), pattern = "LI08872518", value = TRUE))
clearswi_file <- "/data/u_mangels_software/swi/clearswi.jl"
slurm_dir <- file.path(vasculature_dir, "/slurm/clearswi")
if(!dir.exists(slurm_dir)){dir.create(slurm_dir, recursive = TRUE)}

exemplar_subject <- "LI08872518"
# 
for(type in scanner){
  subjects <- subjects_grouped[[type]]
  for(subject in subjects){
     subject_dir <- file.path("/data/pt_02571/Data/preprocessed/vasculature", subject)
    if(!dir.exists(subject_dir)){dir.create(subject_dir, recursive = TRUE)}
    qsm_dir <- sprintf("%s", grep(list.dirs(path = base_dir, recursive = FALSE), 
                                  pattern = sprintf("%s", subject), value = TRUE))
    if(type == "skyra"){
      qsm_dir <- sprintf("%s/bisher/QSM", 
                         grep(list.dirs(path = qsm_dir, recursive = FALSE), 
                                  pattern = sprintf("%s", subject), value = TRUE))
      cmd_3 <- sprintf('julia %s -p %s/phaseAligned.nii -m %s/magnAligned.nii -t[17] -o %s', clearswi_file, 
                     qsm_dir, qsm_dir, subject_dir)
      
    }else{
      qsm_dir <- sprintf("%s/QSM", qsm_dir)
      cmd_3 <- sprintf('julia %s -p %s/tissuePhaseAligned.nii -m %s/magAligned.nii -t[17] -o %s', clearswi_file, 
                     qsm_dir, qsm_dir, subject_dir)
    }
    cmd_1 <- 'export PATH="$PATH:/data/tu_mangels/julia-1.10.0/bin"'
    cmd_2 <- 'export JULIA_DEPOT_PATH="/data/tu_mangels/julia-1.10.0/:$JULIA_DEPOT_PATH"'
    
    fn_sh <- file.path(slurm_dir, sprintf("%s_clearswi_fv.sh", subject))
    
    
    con <- file(fn_sh)
    mem_req <- 5000
    tim_req <- "0-01:00"
    open(con, "w")
    writeLines(c(sprintf("#!/bin/bash\n#"),
                        sprintf("#SBATCH -n 4"),
                        sprintf("#SBATCH -N 1"),
                        sprintf("#SBATCH --mem=%d", mem_req),
                        sprintf("#SBATCH --time=%s", tim_req),
                        sprintf("#SBATCH --job-name %s_swifv", subject),
                        sprintf("#SBATCH --mail-type=begin"),
                        sprintf("#SBATCH --mail-type=end"),
                        sprintf("#SBATCH --mail-type=fail"),
                        sprintf("#SBATCH --mail-user=%s@cbs.mpg.de", system("echo $USER", intern = TRUE)),
                        sprintf("#SBATCH -o %s/%s_clearswi_fv.out", 
                                 slurm_dir, subject),
                        sprintf("#SBATCH -e %s/%s_clearswi_fv.err\n", 
                                 slurm_dir, subject)
                         ), con)
            
            # write the film_gls command to the sbatch file 
            writeLines(cmd_1, con, sep = "\n\n")
            writeLines(cmd_2, con, sep = "\n\n")
            writeLines(cmd_3, con, sep = "\n\n")
            
            # close connection and make sbatch file executable
            close(con)
            system(sprintf("chmod +x %s",fn_sh))
            
            # submit to slurm
            system(sprintf("sbatch %s",fn_sh))
  }
}
```

### Mask
We want to erode the T1 aligned mask 4 times. We will need these masks (with no non-brain voxels) to run the FMMs successfully.
```{r FMM masks}
slurm_dir <- file.path(vasculature_dir, "/slurm/clearswi/masks")
if(!dir.exists(slurm_dir)){dir.create(slurm_dir, recursive = TRUE)}
mem_req <- 5000
tim_req <- "0-01:00"

for(type in scanner){
  for(subject in subjects_grouped[[type]]){
    mask_dir <- sprintf("/data/pt_02571/Data/preprocessed/vasculature/%s/masks", subject)
    if(!dir.exists(subject_dir)){dir.create(subject_dir, recursive = TRUE)}
    cmd_1 <- sprintf("bet %s/%s/clearswi.nii %s/mask_clearswi.nii -m", vasculature_dir, subject, mask_dir)
    cmd_2 <- sprintf("fslmaths %s/mask_clearswi.nii -ero -bin %s/2nd_mask_clearswi.nii",
                       mask_dir, mask_dir)
    # and a second erosion
    cmd_3 <- sprintf("fslmaths %s/2nd_mask_clearswi.nii -ero -bin %s/3rd_mask_clearswi.nii",
                       mask_dir, mask_dir)
    cmd_4 <- sprintf("fslmaths %s/3rd_mask_clearswi.nii -ero -bin %s/4th_mask_clearswi.nii",
                       mask_dir, mask_dir)
    fn_sh <- file.path(slurm_dir, sprintf("%s_FFM_mask.sh", subject))
    
    
    con <- file(fn_sh)
    open(con, "w")
    writeLines(c(sprintf("#!/bin/bash\n#"),
                        sprintf("#SBATCH -n 4"),
                        sprintf("#SBATCH -N 1"),
                        sprintf("#SBATCH --mem=%d", mem_req),
                        sprintf("#SBATCH --time=%s", tim_req),
                        sprintf("#SBATCH --job-name %s_FMMmask", subject),
                        sprintf("#SBATCH --mail-type=begin"),
                        sprintf("#SBATCH --mail-type=end"),
                        sprintf("#SBATCH --mail-type=fail"),
                        sprintf("#SBATCH --mail-user=%s@cbs.mpg.de", system("echo $USER", intern = TRUE)),
                        sprintf("#SBATCH -o %s/%s_FMM_mask.out", 
                                 slurm_dir, subject),
                        sprintf("#SBATCH -e %s/%s_FMM_mask.err\n", 
                                 slurm_dir, subject)
                         ), con)
            
            # write the film_gls command to the sbatch file 
            writeLines(cmd_1, con, sep = "\n\n")
            writeLines(cmd_2, con, sep = "\n\n")
            writeLines(cmd_3, con, sep = "\n\n")
            writeLines(cmd_4, con, sep = "\n\n")
            
            # close connection and make sbatch file executable
            close(con)
            system(sprintf("chmod +x %s",fn_sh))
            
            # submit to slurm
            system(sprintf("sbatch %s",fn_sh))
  }
}
    
    
```

### Registration
We register each SWI to a T1 image