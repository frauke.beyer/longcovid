# (Generalized) Mixed Modelling

## Causal Modelling
Before investigating any potential influences e.g. by regressions, we would like to explore potential causal relationships by having a look at directed acyclic graphs (DAGs) using the R package dagitty (Ref). 

### Intro directed acyclic graphs
However complicated a DAG may appear, there are only four types of variable relations that combine to form all possible paths within a DAG, the four elemental confounds. To investigate a potential causal link between X (the potential cause of interest) and Y (the outcome) we want to shut all backdoors, that is: we want to control for confounders, but we want to control for the right ones. Selecting the wrong one can potentially open paths between variables with no actual causal links between them (collider bias) or change the effect the cause of interest's effect on the outcome (e.g. multicollinearity). Here is how we can figure out which variable to include or exclude given some DAG: 

1) List all paths connecting X (the potential cause) and Y (the outcome).
2) Classify each path by whether it is open or closed. A path is open unless it contains a collider. 
3) Classify each path by whether it is a backdoor path. A backdoor path has an arrow entering X.
4) If there is any open backdoor paths, decide which variable(s) to condition on to close it (if possible). 

(But see also Blom et al. 2018 for dynamical systems (that is, systems for which you have differential equ.))
```{r DAG: elemental confounds}
#TODO: change coordinates for better visualisation
# The Fork
fork_dag <- dagitty("dag {
                  X <- U -> Y}")

ggdag(fork_dag)

# The Pipe
pipe_dag <- dagitty("dag {
                  X-> Z -> Y}")
ggdag(pipe_dag)

# The Collider
collider_dag <- dagitty("dag {
                  X -> U <- Y}")
ggdag(collider_dag)


# The Descendant
descendant_dag <- dagitty("dag {
                  X -> Z <- Y
                  Z -> D}")
ggdag(descendant_dag)
```

#### Sanity checks
You can't check whether a DAG is correct, but what you can check is whether certain assumptions hold true, e.g. conditional independencies. 

```{r DAG: conditional independencies}
```

#### Some simple bayesian multiple regression examples
In case needed I could add examples for Multicollinearity, Post-Treatment-Bias and Collider Bias.

#### Instrumental Variable
In causal terms an instrumental variable is a variable that acts like a natural experiment on the exposure E. 
Given the following dag:
```{r}
```
An instrumental variable Q satisfies following criteria: 

1) Independent of U
2) Not independent of E
3) Q cannot influence W except through E (exclusion restriction)

We will have a look at an example from chapter 14, statistical rethinking.
In this example, we have the following DAG: 
```{r}
sim_data <- tibble(x = rep(1,100), y = rep(1,100), z = rep(1, 100))
edu_dag <- dagitty("dag {
                   Q -> E;
                   E -> W;
                  E <- U -> W}")

ggdag(edu_dag)

#tidy_dagitty(edu_dag)
```

We use the ggdag package to have visualizations and data-handling more similar to tidyverse. 

```{r instrumental variable example}
# First, we simulate a dataset
set.seed(73)
N <- 500
U_sim <- rnorm(N)
Q_sim <- sample(1:4, size = N, replace = TRUE)
E_sim <- rnorm(N, U_sim + Q_sim)
W_sim <- rnorm(N, U_sim + 0 * E_sim)
dat_sim <- list(W = standardize(W_sim),
                E = standardize(E_sim),
                Q = standardize(Q_sim))

```

To investigate various strategies' (how to handle an instrumental variable) effect on the causal link / the regression, have a look at the following three models: 

```{r warning = FALSE, eval = FALSE}
# confound, in this case positively inflated
m_1 <- ulam(
  alist(
    W ~ dnorm(mu, sigma),
    mu <- a_W + b_EW * E,
    a_W ~ dnorm(0,0.2),
    b_EW ~ dnorm(0, 0.5),
    sigma ~ dexp(1)
  ), data = dat_sim, chains = 4, cores = 4)

precis(m_1)

# mess, you should not condition on the instrumental variable
m_2 <- ulam(
  alist(
    W ~ dnorm(mu, sigma),
    mu <- a_W + b_EW * E + b_QW*W,
    a_W ~ dnorm(0,0.2),
    b_EW ~ dnorm(0, 0.5),
    b_QW ~ dnorm(0,0.5),
    sigma ~ dexp(1)
  ), data = dat_sim, chains = 4, cores = 4)
#precis(m_2)

# model with random slopes
m_3 <- ulam(
  alist(
    c(W,E) ~ multi_normal(c(muW, muE), Rho, Sigma),
    muW <- aW + bEW*E,
    muE <- aE + bQE*Q,
    c(aW,aE) ~ normal(0,0.2),
    c(bEW, bQE) ~ normal(0,0.5),
    Rho ~ lkj_corr(2),
    Sigma ~ exponential(1)
  ), data = dat_sim, chains = 4, cores = 4)

precis(m_3, depth = 3)
``` 
```{r warning = FALSE, eval = FALSE}
#m_1x <- ulam(m_1, data = dat_sim, chains = 4, cores = 4)
#m_3x <- ulam(m_3, data = dat_sim, chains = 4, cores = 4)

set.seed(73)
N <-  500
U_sim <- rnorm(N)
Q_sim <- sample(1:4, size = N, replace = TRUE)
E_sim <- rnorm(N, U_sim + Q_sim)
W_sim  <- rnorm(N, -U_sim + 0.2 * E_sim)

dat_sim <- list(W = standardize(W_sim),
                E = standardize(E_sim),
                Q = standardize(Q_sim))

m_1x <- ulam(m_1, data = dat_sim, chains = 4, cores = 4)
m_3x <- ulam(m_3, data = dat_sim, chains = 4, cores = 4)
precis(m_3x, depth = 3)
```
There is actually a way to identify instrumental variable using dagitty

```{r identifying IVs}
dag_IV <- ("dag{Q -> E <- U -> W <- E}")
instrumentalVariables(dag_IV, exposure = "E", outcome = "W")
```
#### Frontdoor criterion
In case needed..

