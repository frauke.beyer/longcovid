# Basic Models with Actual Data

For our various measures of interest we are interested in the group differences - that is a healthy control group, for now defined by number of symptoms < 2, and a LC group. We begin with gray matter differences, in particular differences of thickness as by freesurfer segmentation. Remember, we want to have a look at the following models:
```{=tex}
\usepackage{booktabs}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage{amsmath, amsthm, amssymb, amsfonts}
\usepackage{bbold}
```

\newcommand{\normal}{\mathcal{N}}
\newcommand{\brainN}{X \sim \mathcal{N}(\mu,\,\sigma^{2})}
\newcommand{\ci}{\perp\!\!\!\perp}

$$\begin{equation*}
  B_{i} \sim \mathcal{N}(\mu_{i},\,\sigma) \\
  \mu_{i} = \alpha_{lc[i]}\\
  \alpha_{j} \sim \mathcal{N}(\bar{a}, \sigma_{\alpha})\\
  \bar{a} \sim \mathcal{N}(0,0.2)\\
  \sigma_{\alpha} \sim Exponential(1)\\
  \sigma \sim Exponential(1)
\end{equation*}$$



We will organize the results in lists. The most abstract list will contain wm vs gm vs. qsm. 
Wm will contain results regarding various fixel measures (per region), gm results of Thickness and volume; qsm results regarding iron, vascular density and maybe SvO2. Additionally, FLAIR and / or hemarrhogae results might be added. 

```{r}
basic_results <- list()
gm_basic_results <- list()
gm_regions <- list()
basic_models <- list()
basic_models["basic_model_1"] <- list()
basic_model_2 <- list()
basic_model_3
basic_model_4
```


## Grey Matter - Thickness
 
We will first apply a simple bayesian model with groups as levels. We then continue by additionally having sex as second group. 


### Data
```{r}
subjects <- select_df %>% select(LI_Nr) %>% pull()
gm_df_lh <- read_table("/data/pt_02571/Results/MRI/FREESURFER/aparc_stats_lh.txt") %>% 
  rename(LI_Nr = lh.aparc.thickness) %>% filter(LI_Nr %in% subjects)
names <- gm_df_lh %>% select(starts_with("lh"))
gm_df_rh <- read_table("/data/pt_02571/Results/MRI/FREESURFER/aparc_stats_rh.txt") %>%
  rename(LI_Nr = rh.aparc.thickness) %>% filter(LI_Nr %in% subjects)


gm_rh_df <- merge(select_df, gm_df_rh, by = "LI_Nr") %>%
  mutate(LC = case_when(
    symptoms >= 2 ~ 2,
    symptoms < 2 ~ 1,
    .default = NA
  ), sex = case_when(
    sex == "female" ~ 2,
    sex == "male" ~ 1,
    .default = NA
  )) %>% mutate(rh_entorhinal_thickness = scale(gm_rh_df$rh_entorhinal_thickness)[,1])

#gm_rh_df <- merge(gm_rh_df, age_df, by = "LI_Nr")

gm_lh_df <- merge(select_df, gm_df_lh, by = "LI_Nr") %>%
  mutate(LC = case_when(
    symptoms >= 2 ~ 2,
    symptoms < 2 ~ 1,
    .default = NA
  ), sex = case_when(
    sex == "female" ~ 2,
    sex == "male" ~ 1,
    .default = NA
  )) 
#gm_lh_df <- gm_lh_df %>% mutate(lh_entorhinal_thickness = scale(gm_lh_df$lh_entorhinal_thickness)[,1])

thickness_df <- left_join(gm_rh_df, gm_lh_df, by = "LI_Nr")

names <- thickness_df %>% select(starts_with("lh")) %>% colnames(.) %>% str_split_i(., "_", 2)
for(name in names){
  cols <- grepl(name, colnames(thickness_df))

# Calculate the mean of these columns row-wise
  mean_values <- rowMeans(thickness_df[, cols], na.rm = TRUE)
  thickness_df[name] <- mean_values
}

```
We are interested in the following structures:
medial and lateral orbitofrontal, dorsolateral prefrontal, parahippocampal, entorhinal, pirirhinal, rostral anterior cingulate and the caudal anterior cingulate cortex, the insula and the olfactory bulb. Further, we will extract hippocampus and basal ganglia volumes per hemisphere. Finally, we will investigate thalamic as well as whole brainstem volumes and volumes of regions of interest within the brainstem.

```{r}
basic_thickness_results <- list()
# for(hemisphere in c("rh", "lh")){
#   regions <- sprintf("%s_medialorbitofrontal_thickness", "%s_lateralorbitofrontal_thickness",
#              "%s_parahippocampal_thickness", "%s_caudalanteriorcingulate_thickness",
#              "%s_posteriorcingulate_thickness","%s_rostralanteriorcingulate_thickness",
#              "%s_insula_thickness", "%s_cuneus_thickness", "%s_entorhinal_thickness", 
#              hemisphere, hemisphere, hemisphere,
#              hemisphere, hemisphere, hemisphere, 
#              hemisphere, hemisphere, hemisphere)

rois <- c( "rostralanteriorcingulate", "insula", "caudalanteriorcingulate", "entorhinal", 
          "parahippocampal", "medialorbitofrontal", "lateralorbitofrontal")
dist_matrix <- matrix(nrow = 125, ncol = 125, NA)
  for(i_row in c(1:125)){
    for(j_col in c(1:125)){
      dist_matrix[i_row,j_col] <- abs(thickness_df$age.x[[j_col]] - thickness_df$age.x[[i_row]])
    }
  }
  
Dmat <- dist_matrix/max(dist_matrix)
scan <- thickness_df$skyra.x + 1
lc <- thickness_df$LC.x
sex <- thickness_df$sex.x

for(region in rois){
  outcome <- scale(thickness_df[region])[,1]
  
  tmp_data <- list(C = outcome, lc =  lc)
  m1_a <- ulam(
    alist(
      C ~ dnorm(mu, sigma),
      mu <- a[lc],
      a[lc] ~ dnorm(a_bar,sigma_bar),
      a_bar ~ dnorm(0, 0.2),
      sigma_bar ~ dexp(1),
      sigma ~ dexp(1)
    ), data = tmp_data, chains = 4, cores = 4, cmdstan = TRUE, log_lik = TRUE)
  
  tmp_data <- list(C = outcome, lc =  lc, scan = scan)
  m1_b <- ulam(
    alist(
      C ~ dnorm(mu, sigma),
      mu <- a[lc] + b[scan],
      a[lc] ~ dnorm(a_bar,a_sigma),
      b[scan] ~ dnorm(0, b_sigma),
      a_bar ~ dnorm(0, 0.2),
      a_sigma ~ dexp(1),
      b_sigma ~ dexp(1),
      sigma ~ dexp(1)
    ), data = tmp_data, chains = 4, cores = 4, cmdstan = TRUE, log_lik = TRUE)
  
  tmp_data <- list(C = outcome, lc =  lc, scan = scan, sex = sex)
  
  m1_c <- ulam(
    alist(
      C ~ dnorm(mu, sigma),
      mu <- a[lc] + b[scan] + g[sex],
      a[lc] ~ dnorm(a_bar,a_sigma),
      b[scan] ~ dnorm(0, b_sigma),
      g[sex] ~ dnorm(0, g_sigma),
      a_bar ~ dnorm(0, 0.2),
      a_sigma ~ dexp(1),
      b_sigma ~ dexp(1),
      g_sigma ~ dexp(1),
      sigma ~ dexp(1)
    ), data = tmp_data, chains = 4, cores = 4, cmdstan = TRUE, log_lik = TRUE)
  
  # simple gaussian process - we want to later compare it to combined gaussian process + multilevel and gaussian process by group.... what to do about overfitting?
   tmp_data <- list(C = outcome, lc =  lc, 
                    Dmat = Dmat, N = 125)
   m2_a <- ulam(
    alist(
      C ~ multi_normal(mu, SIGMA),
      mu <- a[lc],
      matrix [N,N]: SIGMA <- cov_GPL2(Dmat, etasq, rhosq, 0.01),
      a[lc] ~ normal(a_bar, a_sigma),
      a_bar ~ dnorm(0, 0.2),
      a_sigma ~ dexp(1),
      etasq ~ half_normal(1, 0.25),
      rhosq ~ half_normal(3, 0.25)
    ), data = tmp_data, chains = 4, cores = 4, cmdstan = TRUE)
  
   tmp_data <- list(C = outcome, lc =  lc, scan = scan, 
                    Dmat = Dmat, N = 125)
   m2_b <- ulam(
    alist(
      C ~ multi_normal(mu, SIGMA),
      mu <- a[lc] + b[lc],
      matrix [N,N]: SIGMA <- cov_GPL2(Dmat, etasq, rhosq, 0.01),
      a[lc] ~ normal(a_bar, a_sigma),
      b[scan] ~ normal(0, b_sigma),
      a_bar ~ dnorm(0, 0.2),
      a_sigma ~ dexp(1),
      b_sigma ~ dexp(1),
      etasq ~ half_normal(1, 0.25),
      rhosq ~ half_normal(3, 0.25)
    ), data = tmp_data, chains = 4, cores = 4, cmdstan = TRUE)
  tmp_data <- list(C = outcome, lc =  lc, scan = scan,
                   sex = sex, Dmat = Dmat, N = 125)
  m2_c <- ulam(
  alist(
    C ~ multi_normal(mu, SIGMA),
    mu <- a[lc] + b[scan] + g[sex],
    matrix [N,N]: SIGMA <- cov_GPL2(Dmat, etasq, rhosq, 0.01),
    a[lc] ~ normal(a_bar, a_sigma),
    b[scan] ~ normal(0, b_sigma),
    g[sex] ~ normal(0, g_sigma),
    a_bar ~ dnorm(0, 0.2),
    a_sigma ~ dexp(1),
    b_sigma ~ dexp(1),
    g_sigma ~ dexp(1),
    etasq ~ half_normal(1, 0.25),
    rhosq ~ half_normal(3, 0.25)
  ), data = tmp_data, chains = 4, cores = 4, cmdstan = TRUE)
  basic_models <- list()
  basic_models[["m1_a"]] <- m1_a
  basic_models[["m1_b"]] <- m1_b
  basic_models[["m1_c"]] <- m1_c
  basic_models[["m2_a"]] <- m2_a
  basic_models[["m2_b"]] <- m2_b
  basic_models[["m3_c"]] <- m2_c
  
  basic_thickness_results[[region]] <- basic_models
    
}
# predefined regions of interest

```

#### Results
```{r}
  precis(basic_thickness_results$entorhinal$m3, depth = 3)

post <- extract.samples(basic_thickness_results$insula$m2_b)
post$diff_a <- post$a[,1] - post$a[,2]
post$diff_b <- post$b[,1] - post$b[,2]
post$diff_g <- post$g[,1] - post$g[,2]
view(precis(post))
```

Lets have a look at the posterior covariance matrix:
```{r}
models <- c("m2_a", "m2_b", "m3_c")
for(region in rois){
  for(model in models){
    post <- extract.samples(basic_thickness_results[[sprintf("%s", region)]][[sprintf("%s", model)]])
    jpeg(file= sprintf("/data/pt_02571/Results/MRI/FREESURFER/thickness_posterior_plots/%s_cov_%s.jpeg",
                       region, model))
    plot(NULL, xlab="distance (age in years)", ylab="covariance",
         xlim=c(0,max(dist_matrix)), ylim=c(0,2.5))
    title(sprintf("%s %s", region, model))
    
    
    x_seq <- seq(from=0, to=max(dist_matrix), length.out = 100)
    pmcov <- sapply(x_seq, function(x) post$etasq*exp(-post$rhosq*(x/max(dist_matrix))^2))
    pmcov_mu <- apply(pmcov, 2, mean)
    lines(x_seq, pmcov_mu, lwd=2)
    
    for(i in seq_along(1:50))
      curve(post$etasq[i]*exp(-post$rhosq[i]*(x/max(dist_matrix))^2), add=TRUE, 
            col=col.alpha("black", 0.3))
    # prior
    eta <- abs(rnorm(1e3, 1, 0.25))
    rho <- abs(rnorm(1e3, 3, 0.25))
    #eta <- rexp(1e3,2)
    #rho <- rexp(1e3,0.5)
    d_seq <- seq(from=0, to=max(dist_matrix), length.out = 100)
    K <- sapply(d_seq, function(x) eta^2*exp(-rho^2*(x/max(dist_matrix))^2))
    lines(d_seq, colMeans(K), lwd=2, col=rangi2)
    shade(apply(K, 2, PI), d_seq)
    dev.off()
  }
}
# WAIC comparison of the three basic models


for(region in rois){
  jpeg(file= sprintf("/data/pt_02571/Results/MRI/FREESURFER/thickness_posterior_plots/compare_%s.jpeg",
                         region))
  plot(compare(basic_thickness_results[[sprintf("%s", region)]][["m1_a"]], basic_thickness_results[[sprintf("%s", region)]][["m1_b"]], basic_thickness_results[[sprintf("%s", region)]][["m1_c"]]))
  dev.off()
}

median_correlations <- list()
for(region in rois){
  post_1 <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m2_a"]])
  post_2 <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m2_b"]])
  post_3 <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m3_c"]])
  K <- list()
  tmp_K_1 <- matrix(nrow = 125, ncol = 125, NA)
  tmp_K_2 <- matrix(nrow = 125, ncol = 125, NA)
  tmp_K_3 <- matrix(nrow = 125, ncol = 125, NA)
  for(i_row in c(1:125)){
    for(j_col in c(1:125)){
      tmp_K_1[i_row,j_col] <- median(post_1$etasq) * exp(-median(post_1$rhosq)*Dmat[i_row, j_col]^2)
      tmp_K_2[i_row,j_col] <- median(post_2$etasq) * exp(-median(post_1$rhosq)*Dmat[i_row, j_col]^2)
      tmp_K_3[i_row,j_col] <- median(post_3$etasq) * exp(-median(post_1$rhosq)*Dmat[i_row, j_col]^2)
    }
  }
  diag(tmp_K_1) <- median(post_1$etasq) + 0.01
  diag(tmp_K_2) <- median(post_2$etasq) + 0.01
  diag(tmp_K_3) <- median(post_1$etasq) + 0.01
  
  Rho_1 <- round(cov2cor(tmp_K_1), 2)
  Rho_2 <- round(cov2cor(tmp_K_2), 2)
  Rho_3 <- round(cov2cor(tmp_K_3), 2)
  
  Rho_1[Rho_1 > 1] <- 1
  Rho_2[Rho_2 > 1] <- 1
  Rho_3[Rho_3 > 1] <- 1
  Rho_1[Rho_1 < -1] <- -1
  Rho_2[Rho_2 < -1] <- -1
  Rho_3[Rho_3 < -1] <- -1
  
  median_correlations[[sprintf("%s", region)]][["R_1"]] <- Rho_1
  median_correlations[[sprintf("%s", region)]][["R_2"]] <- Rho_2
  median_correlations[[sprintf("%s", region)]][["R_3"]] <- Rho_3
}


library(corrplot)
for(region in rois){
  for(correlation_matrix in c("R_1", "R_2", "R_3")){
    jpeg(file = sprintf("/data/pt_02571/Results/MRI/FREESURFER/thickness_posterior_plots/02_advanced_%s_%s_med_corr.jpeg", region, correlation_matrix))
    # Call your plot
  # Assuming 'correlation_matrix' is your correlation matrix
  corrplot(median_correlations[[sprintf("%s", region)]][[correlation_matrix]], method = "color", tl.pos = "n", is.corr = TRUE, type = "lower" )
  
  # Close the device
  dev.off()
  }
  
  
}
```

### Marginalised differences
```{r}
library(tidyverse)
library(cowplot)
precis_plots <- list()
for(region in rois){
  post_1 <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m1_a"]])
  post_2 <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m2_a"]])
  
  post_1$diff_a <- post_1$a[,1] - post_1$a[,2]
  #post_1$diff_b <- post_1$b[,1] - post_1$b[,2]
  post_2$diff_a <- post_2$a[,1] - post_2$a[,2]
  
  tmp_1 <- precis(post_1)
  tmp_2 <- precis(post_2)
  #post_2$diff_b <- post_2$b[,1] - post_2$b[,2]
  #post_3 <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m3_c"]])
  # Create the df used to make the plot
  df <- rbind(tmp_1[6,], tmp_2[7,]) %>%
    # Transform data to tibble
    as_tibble() %>% 
    mutate(labs_y = c("Basic Model", "GP Model"),
           line_type = "solid")
  df$line_type[(df$`5.5%`<0 & df$`94.5%`>0) | (df$`5.5%`>0 & df$`94.5%` <0)] <- "dashed"
    # Add labs for y axis and define linetype according to interval

    # Change labs for y axis from "-" to "vs"
    # mutate_at(vars(labs_y), function(x) gsub(" - ", " vs ", x))
  
  # Minimum value in the x axis to set the plot limits, as well as the left axis line
  # see second geom_vline. This can probably be calculated from the data.
  min_val <- -1
  
  
  
  # plot
  precis_plots[[sprintf("%s", region)]] <- df %>%
    ggplot(aes(x = mean,
           y = labs_y)) +
    # Add point for estimate
    geom_point()+
    # Add error bars, use line_typ to set line type
    geom_errorbar(aes(xmax = mean + sd,
                      xmin = mean - sd,
                      lty = line_type),
                  show.legend = F,
                  width = 0.3) +
    # Add estimates values as text
    geom_text(aes(label = round(mean, 2)), 
              # nudge position in x and y directions
              nudge_x = 0.1,
              nudge_y = 0.1) +
    # Add dashed vertical line in 0 Difference (y-axis)
    geom_vline(xintercept=0, lty = "dashed") +
    # Make line for second axis (left side)
    geom_vline(xintercept = min_val )  +
    # Put x axis in the right side and make second axis in bottom
    scale_x_continuous(position = "top",
                       sec.axis = sec_axis(~., name = ""),
                       limits = c(min_val,1),
                       expand = c(0,0)) +
    # Change y axis position and add labs
    scale_y_discrete(position = "right",
                     labels = df$labs_y) +
    # Change axis titles
    labs(x = "Difference", y = "") +
    ggtitle(sprintf("%s", region)) +
    # Add cowplot theme
    theme_cowplot()+
    # Final theme adjustments
    # Remove ticks and text from bottom secondary axis
    theme(axis.ticks.x.bottom = element_blank(),
          axis.text.x.bottom = element_blank(),
          axis.line = element_line(colour = "black"))
  
}

for(region in rois){
  jpeg(file = sprintf("/data/pt_02571/Results/MRI/FREESURFER/thickness_posterior_plots/03_1-2_precis_%s.jpeg", region))
  print(precis_plots[[region]])
  dev.off()
}

# we also would like to have summary plots 

df <- tibble(
  id = numeric(),
  mean = numeric(),
  sd = numeric(),
  `5.5%` = numeric(),
  `94.5%` = numeric(),
  histogram = character(),
  y_label = character()
)
for(region in rois){
  post_1 <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m1_a"]])
  post_2 <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m2_a"]])
  
  post_1$diff_a <- post_1$a[,1] - post_1$a[,2]
  #post_1$diff_b <- post_1$b[,1] - post_1$b[,2]
  post_2$diff_a <- post_2$a[,1] - post_2$a[,2]
  
  tmp_1 <- precis(post_1)
  tmp_2 <- precis(post_2)
  #post_2$diff_b <- post_2$b[,1] - post_2$b[,2]
  #post_3 <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m3_c"]])
  # Create the df used to make the plot
  tmp_df <- rbind(tmp_1[6,], tmp_2[7,]) %>% as_tibble() %>% 
    mutate(y_label = c(sprintf("%s M1", str_trunc(string=region, side = "right", width = 8)), sprintf("%s M2", str_trunc(string=region, side = "right", width = 8))))
  df <- rbind(df, tmp_df) %>%
    # Transform data to tibble
    as_tibble() 
}
  
df <- df %>% 
    mutate(line_type = "solid")
df$line_type[(df$`5.5%`<0 & df$`94.5%`>0) | (df$`5.5%`>0 & df$`94.5%` <0)] <- "dashed"
    # Add labs for y axis and define linetype according to interval


    # Change labs for y axis from "-" to "vs"
    # mutate_at(vars(labs_y), function(x) gsub(" - ", " vs ", x))
  
  # Minimum value in the x axis to set the plot limits, as well as the left axis line
  # see second geom_vline. This can probably be calculated from the data.
  min_val <- -1
  
 jpeg(file = sprintf("/data/pt_02571/Results/MRI/FREESURFER/thickness_posterior_plots/01_overview.jpeg"))
  
  # plot
   df %>%
    ggplot(aes(x = mean,
           y = y_label)) +
    # Add point for estimate
    geom_point()+
    # Add error bars, use line_typ to set line type
    geom_errorbar(aes(xmax = mean + sd,
                      xmin = mean - sd,
                      lty = line_type),
                  show.legend = F,
                  width = 0.3) +
    # Add estimates values as text
    geom_text(aes(label = round(mean, 2)), 
              # nudge position in x and y directions
              nudge_x = 0.1,
              nudge_y = 0.1) +
    # Add dashed vertical line in 0 Difference (y-axis)
    geom_vline(xintercept=0, lty = "dashed") +
    # Make line for second axis (left side)
    geom_vline(xintercept = min_val )  +
    # Put x axis in the right side and make second axis in bottom
    scale_x_continuous(position = "top",
                       sec.axis = sec_axis(~., name = ""),
                       limits = c(min_val,1),
                       expand = c(0,0)) +
    # Change y axis position and add labs
    scale_y_discrete(position = "right") +
    # Change axis titles
    labs(x = "stand. diff.", y = "") +
    ggtitle("Overview") +
    # Add cowplot theme
    theme_cowplot()+
    # Final theme adjustments
    # Remove ticks and text from bottom secondary axis
    theme(axis.ticks.x.bottom = element_blank(),
          axis.text.x.bottom = element_blank(),
          axis.line = element_line(colour = "black"))
   dev.off()
  

```
Next, let us visualize the advanced models in comparison:
```{r}
# plot for HC vs. LC effects, advanced models
df <- tibble(
  id = numeric(),
  mean = numeric(),
  sd = numeric(),
  `5.5%` = numeric(),
  `94.5%` = numeric(),
  histogram = character(),
  y_label = character(),
  model = character()
)
for(region in rois){
  post_2_a <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m2_a"]])
  post_2_b <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m2_b"]])
  post_2_c <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m3_c"]])
  
  post_2_a$diff_a <- post_2_a$a[,1] - post_2_a$a[,2]
  #post_1$diff_b <- post_1$b[,1] - post_1$b[,2]
  post_2_b$diff_a <- post_2_b$a[,1] - post_2_b$a[,2]
  post_2_c$diff_a <- post_2_c$a[,1] - post_2_c$a[,2]
  
  tmp_1 <- precis(post_2_a)
  tmp_2 <- precis(post_2_b)
  tmp_3 <- precis(post_2_c)
  #post_2$diff_b <- post_2$b[,1] - post_2$b[,2]
  #post_3 <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m3_c"]])
  # Create the df used to make the plot
  tmp_df <- rbind(tmp_1[7,], tmp_2[10,], tmp_3[13,]) %>% as_tibble() %>% 
    mutate(y_label = c(sprintf("%s M2a", str_trunc(string=region, side = "right", width = 8)), sprintf("%s M2b", str_trunc(string=region, side = "right", width = 8)), sprintf("%s M2c", str_trunc(string=region, side = "right", width = 8))), model = c("M2a", "M2b", "M2c"))
  df <- rbind(df, tmp_df) %>%
    # Transform data to tibble
    as_tibble() 
}
  
df <- df %>% 
    mutate(line_type = "solid")
df$line_type[(df$`5.5%`<0 & df$`94.5%`>0) | (df$`5.5%`>0 & df$`94.5%` <0)] <- "dashed"
    # Add labs for y axis and define linetype according to interval


    # Change labs for y axis from "-" to "vs"
    # mutate_at(vars(labs_y), function(x) gsub(" - ", " vs ", x))
  
  # Minimum value in the x axis to set the plot limits, as well as the left axis line
  # see second geom_vline. This can probably be calculated from the data.
  min_val <- min(df$`5.5%`)
  max_val <- max(df$`94.5%`)
  
 jpeg(file = sprintf("/data/pt_02571/Results/MRI/FREESURFER/thickness_posterior_plots/01_overview_advanced.jpeg"))
  
  # plot
   df %>%
    ggplot(aes(x = mean,
           y = y_label)) +
    # Add point for estimate
    geom_point()+
    # Add error bars, use line_typ to set line type
    geom_errorbar(aes(xmax = mean + sd,
                      xmin = mean - sd,
                      lty = line_type,
                      color = factor(model)),
                  show.legend = F,
                  width = 0.3) +
    # Add estimates values as text
    geom_text(aes(label = round(mean, 2)), 
              # nudge position in x and y directions
              nudge_x = 0.1,
              nudge_y = 0.1) +
    # Add dashed vertical line in 0 Difference (y-axis)
    geom_vline(xintercept=0, lty = "dashed") +
    # Make line for second axis (left side)
    geom_vline(xintercept = min_val )  +
    # Put x axis in the right side and make second axis in bottom
    scale_x_continuous(position = "top",
                       sec.axis = sec_axis(~., name = ""),
                       limits = c(min_val,max_val),
                       expand = c(0,0)) +
    # Change y axis position and add labs
    scale_y_discrete(position = "right") +
    # Change axis titles
    labs(x = "stand. diff.", y = "") +
    ggtitle("Overview") +
    # Add cowplot theme
    theme_cowplot()+
    # Final theme adjustments
    # Remove ticks and text from bottom secondary axis
    theme(axis.ticks.x.bottom = element_blank(),
          axis.text.x.bottom = element_blank(),
          axis.line = element_line(colour = "black"))
   dev.off()

## plot for scanner differences ##
   ############################
   
df <- tibble(
  id = numeric(),
  mean = numeric(),
  sd = numeric(),
  `5.5%` = numeric(),
  `94.5%` = numeric(),
  histogram = character(),
  y_label = character(),
  model = character()
)

for(region in rois){
  post_2_b <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m2_b"]])
  post_2_c <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m3_c"]])
  
  post_2_b$diff_b <- post_2_b$b[,1] - post_2_b$b[,2]
  post_2_c$diff_b <- post_2_c$b[,1] - post_2_c$b[,2]
  
  tmp_2 <- precis(post_2_b)
  tmp_3 <- precis(post_2_c)
  #post_2$diff_b <- post_2$b[,1] - post_2$b[,2]
  #post_3 <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m3_c"]])
  # Create the df used to make the plot
  tmp_df <- rbind(tmp_2[10,], tmp_3[13,]) %>% as_tibble() %>% 
    mutate(y_label = c(sprintf("%s M2b", str_trunc(string=region, side = "right", width = 8)), sprintf("%s M2c", str_trunc(string=region, side = "right", width = 8))), model = c("M2b", "M2c"))
  df <- rbind(df, tmp_df) %>%
    # Transform data to tibble
    as_tibble() 
}

  
df <- df %>% 
    mutate(line_type = "solid")
df$line_type[(df$`5.5%`<0 & df$`94.5%`>0) | (df$`5.5%`>0 & df$`94.5%` <0)] <- "dashed"
    # Add labs for y axis and define linetype according to interval


    # Change labs for y axis from "-" to "vs"
    # mutate_at(vars(labs_y), function(x) gsub(" - ", " vs ", x))
  
  # Minimum value in the x axis to set the plot limits, as well as the left axis line
  # see second geom_vline. This can probably be calculated from the data.
  min_val <- min(df$`5.5%`)
  max_val <- max(df$`94.5%`)
  
 jpeg(file = sprintf("/data/pt_02571/Results/MRI/FREESURFER/thickness_posterior_plots/01_overview_scanner.jpeg"))
  
  # plot
   df %>%
    ggplot(aes(x = mean,
           y = y_label)) +
    # Add point for estimate
    geom_point()+
    # Add error bars, use line_typ to set line type
    geom_errorbar(aes(xmax = mean + sd,
                      xmin = mean - sd,
                      lty = line_type,
                      color = factor(model)),
                  show.legend = F,
                  width = 0.3) +
    # Add estimates values as text
    geom_text(aes(label = round(mean, 2)), 
              # nudge position in x and y directions
              nudge_x = 0.1,
              nudge_y = 0.1) +
    # Add dashed vertical line in 0 Difference (y-axis)
    geom_vline(xintercept=0, lty = "dashed") +
    # Make line for second axis (left side)
    geom_vline(xintercept = min_val )  +
    # Put x axis in the right side and make second axis in bottom
    scale_x_continuous(position = "top",
                       sec.axis = sec_axis(~., name = ""),
                       limits = c(min_val,max_val),
                       expand = c(0,0)) +
    # Change y axis position and add labs
    scale_y_discrete(position = "right") +
    # Change axis titles
    labs(x = "stand. diff.", y = "") +
    ggtitle("Overview") +
    # Add cowplot theme
    theme_cowplot()+
    # Final theme adjustments
    # Remove ticks and text from bottom secondary axis
    theme(axis.ticks.x.bottom = element_blank(),
          axis.text.x.bottom = element_blank(),
          axis.line = element_line(colour = "black"))
   dev.off()
   
## plots for sex differences ##
   #########################

df <- tibble(
  id = numeric(),
  mean = numeric(),
  sd = numeric(),
  `5.5%` = numeric(),
  `94.5%` = numeric(),
  histogram = character(),
  y_label = character(),
  model = character()
)

for(region in rois){
  post_2_c <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m3_c"]])
  
  post_2_c$diff_g <- post_2_c$g[,1] - post_2_c$g[,2]
  
  tmp_3 <- precis(post_2_c)
  #post_2$diff_b <- post_2$b[,1] - post_2$b[,2]
  #post_3 <- extract_post_ulam(basic_thickness_results[[sprintf("%s", region)]][["m3_c"]])
  # Create the df used to make the plot
  tmp_df <- tmp_3[13,] %>% as_tibble() %>% 
    mutate(y_label = sprintf("%s M2c", str_trunc(string=region, side = "right", width = 8)), model = "M2c")
  df <- rbind(df, tmp_df) %>%
    # Transform data to tibble
    as_tibble() 
}

  
df <- df %>% 
    mutate(line_type = "solid")
df$line_type[(df$`5.5%`<0 & df$`94.5%`>0) | (df$`5.5%`>0 & df$`94.5%` <0)] <- "dashed"
    # Add labs for y axis and define linetype according to interval


    # Change labs for y axis from "-" to "vs"
    # mutate_at(vars(labs_y), function(x) gsub(" - ", " vs ", x))
  
  # Minimum value in the x axis to set the plot limits, as well as the left axis line
  # see second geom_vline. This can probably be calculated from the data.
  min_val <- min(df$`5.5%`)
  max_val <- max(df$`94.5%`)
  
 jpeg(file = sprintf("/data/pt_02571/Results/MRI/FREESURFER/thickness_posterior_plots/01_overview_sex.jpeg"))
  
  # plot
   df %>%
    ggplot(aes(x = mean,
           y = y_label)) +
    # Add point for estimate
    geom_point()+
    # Add error bars, use line_typ to set line type
    geom_errorbar(aes(xmax = mean + sd,
                      xmin = mean - sd,
                      lty = line_type,
                      color = factor(model)),
                  show.legend = F,
                  width = 0.3) +
    # Add estimates values as text
    geom_text(aes(label = round(mean, 2)), 
              # nudge position in x and y directions
              nudge_x = 0.1,
              nudge_y = 0.1) +
    # Add dashed vertical line in 0 Difference (y-axis)
    geom_vline(xintercept=0, lty = "dashed") +
    # Make line for second axis (left side)
    geom_vline(xintercept = min_val )  +
    # Put x axis in the right side and make second axis in bottom
    scale_x_continuous(position = "top",
                       sec.axis = sec_axis(~., name = ""),
                       limits = c(min(-0.5,min_val),max(max_val, 0.5)),
                       expand = c(0,0)) +
    # Change y axis position and add labs
    scale_y_discrete(position = "right") +
    # Change axis titles
    labs(x = "stand. diff.", y = "") +
    ggtitle("Overview") +
    # Add cowplot theme
    theme_cowplot()+
    # Final theme adjustments
    # Remove ticks and text from bottom secondary axis
    theme(axis.ticks.x.bottom = element_blank(),
          axis.text.x.bottom = element_blank(),
          axis.line = element_line(colour = "black"))
   dev.off()


```



#### Basic Models
```{r}

test <- gm_rh_df %>% select(rh_entorhinal_thickness, LC)
m1_a <- ulam(
  alist(
    rh_entorhinal_thickness ~ dnorm(mu, sigma),
    mu <- a[LC],
    a[LC] ~ dnorm(a_bar,sigma_bar),
    a_bar ~ dnorm(0, 0.2),
    sigma_bar ~ dexp(1),
    sigma ~ dexp(1)
  ), data = test, chains = 4, cores = 4, cmdstan = TRUE)

test <- gm_rh_df %>% select(rh_entorhinal_thickness, LC, sex)

m1_b <- ulam(
  alist(
    rh_entorhinal_thickness ~ dnorm(mu, sigma),
    mu <- a[LC] + g[sex],
    a[LC] ~ dnorm(0,sigma_a),
    g[sex] ~ dnorm(g_bar, sigma_g),
    g_bar ~ dnorm(0, 0.2),
    sigma_a ~ dexp(1),
    sigma_g ~ dexp(1),
    sigma ~ dexp(1)
  ), data = test, chains = 4, cores = 4, cmdstan = TRUE)

test <- gm_lh_df %>% select(lh_entorhinal_thickness, LC)
m1_c <- ulam(
  alist(
    lh_entorhinal_thickness ~ dnorm(mu, sigma),
    mu <- a[LC],
    a[LC] ~ dnorm(a_bar,sigma_bar),
    a_bar ~ dnorm(0, 0.2),
    sigma_bar ~ dexp(1),
    sigma ~ dexp(1)
  ), data = test, chains = 4, cores = 4, cmdstan = TRUE)

test <- gm_lh_df %>% select(lh_entorhinal_thickness, LC, sex)

m1_d <- ulam(
  alist(
    lh_entorhinal_thickness ~ dnorm(mu, sigma),
    mu <- a[LC] + g[sex],
    a[LC] ~ dnorm(0,sigma_a),
    g[sex] ~ dnorm(g_bar, sigma_g),
    g_bar ~ dnorm(0, 0.2),
    sigma_a ~ dexp(1),
    sigma_g ~ dexp(1),
    sigma ~ dexp(1)
  ), data = test, chains = 4, cores = 4, cmdstan = TRUE)
```
#### Gaussian Process

```{r}
dist_matrix <- matrix(nrow = 125, ncol = 125, NA)
for(i_row in c(1:125)){
  for(j_col in c(1:125)){
    dist_matrix[i_row,j_col] <- abs(gm_rh_df$age[[j_col]] - gm_rh_df$age[[i_row]])
  }
}

Dmat <- dist_matrix/max(dist_matrix)

test <- gm_rh_df %>% select(rh_entorhinal_thickness, )
# simple gaussian process - we want to later compare it to combined gaussian process + multilevel and gaussian process by group.... what to do about overfitting?

sim_data <- list(C = gm_rh_df$rh_entorhinal_thickness, lc =  gm_rh_df$LC,
                 sex = gm_rh_df$sex, Dmat = Dmat, N = 125)
m_C_5 <- ulam(
  alist(
    C ~ multi_normal(mu, SIGMA),
    mu <- a[group],
    matrix [N,N]: SIGMA <- cov_GPL2(Dmat, etasq, rhosq, 0.01),
    a[group] ~ normal(0, 0.1),
    etasq ~ half_normal(1, 0.25),
    rhosq ~ half_normal(3, 0.25)
  ), data = sim_data, chains = 4, cores = 4)

m_C_5 <- ulam(
  alist(
    C ~ multi_normal(mu, SIGMA),
    mu <- a[group] + g[sex],
    matrix [N,N]: SIGMA <- cov_GPL2(Dmat, etasq, rhosq, 0.01),
    a[group] ~ normal(0, 0.1),
    etasq ~ half_normal(1, 0.25),
    rhosq ~ half_normal(3, 0.25)
  ), data = sim_data, chains = 4, cores = 4)
```


### Visualizing results
```{r}
precis(m1_a, depth = 3)

post <- extract_post_ulam(m1_a)
post$diff_a <- post$a[,1] - post$a[,2]
view(precis(post))


precis(m1_b, depth = 3)

post <- extract_post_ulam(m1_b)
post$diff_a <- post$a[,1] - post$a[,2]
post$diff_g <- post$g[,1] - post$g[,2]
view(precis(post))

precis(m1_c, depth = 3)

post <- extract_post_ulam(m1_c)
post$diff_a <- post$a[,1] - post$a[,2]
view(precis(post))


precis(m1_d, depth = 3)

post <- extract_post_ulam(m1_d)
post$diff_a <- post$a[,1] - post$a[,2]
post$diff_g <- post$g[,1] - post$g[,2]
view(precis(post))
```