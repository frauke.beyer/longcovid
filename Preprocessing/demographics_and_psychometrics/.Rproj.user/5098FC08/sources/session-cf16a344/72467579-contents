# Fixel Based Analysis

We start with the preprocessing steps as stated in
https://mrtrix.readthedocs.io/en/dev/fixel_based_analysis/mt_fibre_density_cross-section.html#reduce-biases-in-tractogram-densities
Since we do not have multi-shell DWI we will use MRTrixTissue a fork of MRTrix 3 to estimate the respective response functions (ss3beta function). For now, we leave steps 1 to 4 out (to be added in the future) and start with step 4:

## Dataframe
We need to distinguish between verio and skyra data first, since we would like to estimate average response functions separately for each type of scanner. We focus on the data already provided by LIFE, for now.
Subject LI08862673 is seemingly not processed / had no MRI session and is therefore excluded.

```{r data}
# import data with dates - we know that scanner types changes after 05.12.2022
dates_df <- read.table("/data/pt_02571/Data/preprocessed/fixel_analysis/PV712_ids_dates", sep = ",", header = TRUE)
dates_df <- merge(LC_df, as_tibble(dates_df), by = "LI_Nr")

# make a df for differentiating skyra vs. verio. 
# We will also use this df to select suitable candidates for the FOD template.
# for dates, > sign is translated into >= sign - don't know why...
select_df <- tibble(prolonged_symptoms = dates_df$LC_REHABNEQ_F0055.x,
       symptoms = dates_df$`LC_#symptoms`, cov_pos = dates_df$Cov_pos,
       duration = dates_df$LC_STATUS_DURATION, date = dates_df$Exp_Date.y, 
       sex = dates_df$sex_score, 
       age = dates_df$age_score,
       LI_Nr = dates_df$LI_Nr) %>% 
  mutate(skyra = case_when(date > "2022-12-06" ~ 1, .default = 0))  %>%
  mutate(symptoms = as.character(symptoms)) %>% mutate(symptoms = as.numeric(symptoms)) %>%
  filter(LI_Nr != "LI08862673")



#cut(LC_df$age_score, 
                   #breaks=c(-Inf, 28, 38, 48, 58, 68, 78, Inf))

```

## Average response function {-}
We start with computing the average response functions. For this, we will follow 
Meisler, Steven Lee, and John DE Gabrieli. “Fiber-Specific Structural Properties Relate to Reading Skills in Children and Adolescents.” ELife 11 (December 28, 2022): e82088. https://doi.org/10.7554/eLife.82088.
in computing the response functions separately (see also forum of MRtrix).

```{r}
#first tryout
clean <- "ls **/@(eddy_corrected.mif|eddy_corrected_unbiased.mif|eddy_corrected_unbiased_nan.mif|fod_in_template_space_NOT_REORIENTED.mif|T1_brain_mask.mif|T1_upsampled_brain_mask.mif|T1_upsampled_brain_mask.nii.gz|bet2_upsampled.nii.gz|bet2_upsampled_mask.nii.gz|bet2_upsampled_mask.mif)
"
#then delete
clean <- "command rm -r **/@(fixel_in_template_space_NOT_REORIENTED|eddy_corrected.mif|eddy_corrected_unbiased.mif|eddy_corrected_unbiased_nan.mif|fod_in_template_space_NOT_REORIENTED.mif|T1_brain_mask.mif|T1_upsampled_brain_mask.mif|T1_upsampled_brain_mask.nii.gz|bet2_upsampled.nii.gz|bet2_upsampled_mask.nii.gz|bet2_upsampled_mask.mif)
"
```

```{r average response}

# main directory for tissue analysis
fixel_dir <- "/data/pt_02571/Analysis/longcovid/Preprocessing/fixel_analysis"

verio_subjects <- select_df %>% filter(skyra == 0) %>% select(LI_Nr) %>% pull()
skyra_subjects <- select_df %>% filter(skyra == 1) %>% select(LI_Nr) %>% pull()
subjects_grouped <- list()
subjects_grouped[["verio"]] <- verio_subjects
subjects_grouped[["skyra"]] <- skyra_subjects

run_in_R <- TRUE
for(tissue in c("gm", "wm", "csf")){
  for(type in c("verio", "skyra")){
    paths <- sprintf("/data/pt_02571/Data/preprocessed/nipype/diffusion/%s/response_%s.txt",
                               subjects_grouped[[type]], tissue)
    cmd <- sprintf("responsemean %s /data/pt_02571/Data/preprocessed/fixel_analysis/%s_group_average_response_%s -force", paste(paths, collapse = " "), type, tissue)
    
    fn_sh <- file.path(fixel_dir, sprintf("%s_group_average_response_%s.sh", type, tissue))
    
    con <- file(fn_sh)
    open(con, "w")
    
    # write the film_gls command to the sbatch file
    writeLines(cmd, con, sep = "\n\n")
    
    # close connection and make sbatch file executable
    close(con) 
    if(run_in_R == TRUE){
      system(sprintf("sh %s",fn_sh))
    }
  }
  
}


```

And we want to save the list of participants for other user cases
```{r}
for(type in c("verio", "skyra")){
    id_path <- sprintf("/data/pt_02571/Data/Tables/"
                               )
    cmd <- sprintf("%s", paste(subjects_grouped[[type]], collapse = "\n"))
    fn_sh <- file.path(id_path, sprintf("%s_subjects.txt", type))
    
    con <- file(fn_sh)
    open(con, "w")
    
    # write the film_gls command to the sbatch file
    writeLines(cmd, con, sep = "")
    
    # close connection and make sbatch file executable
    close(con)
}

```

## Single-Shell-3-Tissue (step 7) {-}

In case you have single-shell data make sure to install MRtrix3Tissue first (MRtrix fork by Dhollander) and use the ss3t command.
```{r}

path <- "/data/pt_02571/Data/preprocessed/nipype/diffusion"
fixel_output <- "/data/pt_02571/Data/preprocessed/fixel_analysis"

for(type in c("verio", "skyra")){
  subjects <- subjects_grouped[[type]]
    for(subject in subjects){
    
    
    cmd <- sprintf("ss3t_csd_beta1 %s/%s/eddy_corrected_unbiased_upsampled.mif %s/%s_group_average_response_wm %s/%s/wmfod.mif %s/%s_group_average_response_gm %s/%s/gm.mif  %s/%s_group_average_response_csf %s/%s/csf.mif -mask %s/%s/eddy_dwi_mask_upsampled.mif -force", 
                   path, subject, fixel_output, type, path, subject, fixel_output,
                   type, path, subject, fixel_output, type, path, subject, path, subject)
    #system(cmd)
    #write set up part of sbatch file
    slurm_dir <- file.path(fixel_dir, "ss3t_slurm")
    if(!dir.exists(slurm_dir)){dir.create(slurm_dir, recursive = TRUE)}
    fn_sh <-  file.path(slurm_dir, sprintf("fod_%s_%s_ss3t.sh", subject, type))
    mem_req <- 8000
    tim_req <- "0-02:00"
    con <- file(fn_sh)
    open(con, "w")
            writeLines(c(sprintf("#!/bin/bash\n#"),
                         sprintf("#SBATCH -n 4"),
                         sprintf("#SBATCH -N 1"),
                         sprintf("#SBATCH --mem=%d", mem_req),
                         sprintf("#SBATCH --time=%s", tim_req),
                         sprintf("#SBATCH --job-name %s_%s_ss3t", subject, type),
                         sprintf("#SBATCH --mail-type=begin"),
                         sprintf("#SBATCH --mail-type=end"),
                         sprintf("#SBATCH --mail-type=fail"),
                         sprintf("#SBATCH --mail-user=%s@cbs.mpg.de", system("echo $USER", 
                                                                             intern = TRUE)),
            sprintf("#SBATCH -o %s/%s_%s_ss3t.out",
                    slurm_dir, subject, type),
            sprintf("#SBATCH -e %s/%s_%s_33t.err\n",
                    slurm_dir, subject, type)
            ), con)
  
            # write the film_gls command to the sbatch file
            writeLines(cmd, con, sep = "\n\n")
  
            # close connection and make sbatch file executable
            close(con)
            system(sprintf("chmod +x %s",fn_sh))
  
            # submit to slurm
            system(sprintf("sbatch %s",fn_sh))
    }
}

```

## MT normalize (step 8) {-}

```{r erode}
# For mtnormalize it is advised to have as little no-brain voxels within the mask as possible. Therefore you may want to erode your mask before applying mtnormalize
erode <- FALSE

if(erode == TRUE){
  slurm_dir <- file.path(fixel_dir, "erode_slurm")
  if(!dir.exists(slurm_dir)){dir.create(slurm_dir, recursive = TRUE)}
  
  subjects <- select_df %>% filter(!(LI_Nr %in% c("LI08862673"))) %>% 
  select(LI_Nr) %>% pull
  
  for(subject in subjects){
    cmd_1 <- sprintf("fslmaths %s/%s/eddy_dwi_mask_upsampled.nii.gz -ero -bin %s/%s/eroded_mask_upsampled.nii.gz", path, subject, path, subject)
    # and a second erosion
    cmd_2 <- sprintf("fslmaths %s/%s/eroded_mask_upsampled.nii.gz -ero -bin %s/%s/2nd_eroded_mask_upsampled.nii.gz", path, subject, path, subject)
    
    cmd_3 <- sprintf("mrconvert /data/pt_02571/Data/preprocessed/nipype/diffusion/%s/2nd_eroded_mask_upsampled.nii.gz /data/pt_02571/Data/preprocessed/nipype/diffusion/%s/2nd_eroded_mask_upsampled.mif -force", 
                     subject, subject)
    
    fn_sh <-  file.path(slurm_dir, sprintf("%s_%s_ero.sh", subject, "masks"))
    mem_req <- 5000
    tim_req <- "0-01:00"
    con <- file(fn_sh)
    
    con <- file(fn_sh)
      open(con, "w")
              writeLines(c(sprintf("#!/bin/bash\n#"),
                           sprintf("#SBATCH -n 4"),
                           sprintf("#SBATCH -N 1"),
                           sprintf("#SBATCH --mem=%d", mem_req),
                           sprintf("#SBATCH --time=%s", tim_req),
                           sprintf("#SBATCH --job-name %s_erode", subject),
                           sprintf("#SBATCH --mail-type=begin"),
                           sprintf("#SBATCH --mail-type=end"),
                           sprintf("#SBATCH --mail-type=fail"),
                           sprintf("#SBATCH --mail-user=%s@cbs.mpg.de", system("echo $USER", 
                                                                               intern = TRUE)),
              sprintf("#SBATCH -o %s/%s_erode.out",
                      slurm_dir, subject),
              sprintf("#SBATCH -e %s/%s_erode.err\n",
                      slurm_dir, subject)
              ), con)
    
              # write the film_gls command to the sbatch file
              writeLines(cmd_1, con, sep = "\n\n")
              writeLines(cmd_2, con, sep = "\n\n")
              writeLines(cmd_3, con, sep = "\n\n")
    
              # close connection and make sbatch file executable
              close(con)
              #system(sprintf("chmod +x %s",fn_sh))
    
              # submit to slurm
              #system(sprintf("sbatch %s",fn_sh))
    
  }
}

```

Next, we continue with actually applying mtnormalise
```{r}

subjects <- select_df %>% select(LI_Nr) %>% pull()
    for(subject in subjects){
    
    
      cmd <- sprintf("mtnormalise %s/%s/wmfod.mif %s/%s/wmfod_norm.mif %s/%s/gm.mif %s/%s/gm_norm.mif %s/%s/csf.mif %s/%s/csf_norm.mif -mask %s/%s/2nd_eroded_mask_upsampled.mif -force", 
                     path, subject, path, subject, path, 
                     subject, path, subject, path, 
                     subject, path, subject, path, subject)
      #system(cmd)
      #write set up part of sbatch file
      slurm_dir <- file.path(fixel_dir, "mtnorm_slurm")
      if(!dir.exists(slurm_dir)){dir.create(slurm_dir, recursive = TRUE)}
      fn_sh <-  file.path(slurm_dir, sprintf("%s_mtnorm.sh", subject))
      mem_req <- 16000
      tim_req <- "0-02:00"
      con <- file(fn_sh)
      open(con, "w")
            writeLines(c(sprintf("#!/bin/bash\n#"),
                         sprintf("#SBATCH -n 4"),
                         sprintf("#SBATCH -N 1"),
                         sprintf("#SBATCH --mem=%d", mem_req),
                         sprintf("#SBATCH --time=%s", tim_req),
                         sprintf("#SBATCH --job-name %s_mtnorm", subject),
                         sprintf("#SBATCH --mail-type=begin"),
                         sprintf("#SBATCH --mail-type=end"),
                         sprintf("#SBATCH --mail-type=fail"),
                         sprintf("#SBATCH --mail-user=%s@cbs.mpg.de", system("echo $USER", 
                                                                             intern = TRUE)),
            sprintf("#SBATCH -o %s/%s_mtnorm.out",
                    slurm_dir, subject),
            sprintf("#SBATCH -e %s/%s_mtnorm.err",
                    slurm_dir, subject)
            ), con)
  
            # write the film_gls command to the sbatch file
            writeLines(cmd, con, sep = "\n\n")
  
            # close connection and make sbatch file executable
            close(con)
          system(sprintf("chmod +x %s",fn_sh))
  
          # submit to slurm
          system(sprintf("sbatch %s",fn_sh))
    }

```

## Creating the FOD template

We begin by selecting the individuals for the template. We look for 4 individuals per Scanner per age range, for ranges 19-39; 40 - 49; 50 - 59; 60 - 69. No participant was older than 69. We further looked for 2 LC and 2 noLC participants per cat. for each with one male and one female, respectively. Since we didn't get another male-female pairing for individuals in the skyra scanner aged >=60, we had only 2 individuals for the respective skyra - category with f64p3yLC and m66p9ynoLC. We added two skyra participants with no symptoms from the age range 40-50, f580n und m570n, to make sure to have a balanced number of participants per scanner getting into the template. We have a total of 32 individuals for our template.
```{r}

FOD_template <- c("LI08775630", "LI02568858", "LI08825599", "LI08822236", "LI03925611", "LI03338639" ,"LI08832554", "LI08848714", "LI08842654", "LI00244270", "LI08791878", "LI04360775", "LI08777835",
                  "LI08777570", "LI08837517", "LI04122258", "LI08777872", "LI0266387X", "LI08821732",
                  "LI08834995", "LI00995633", "LI00234157", "LI08921295", "LI08897114", "LI08825575",
                  "LI01872258", "LI03678936", "LI08791854", "LI00573018", "LI00643115", "LI08888219",
                  "LI08928512")

fod_df <- select_df %>%
  filter((LI_Nr %in% FOD_template))

```


Next, we link masks and input to have all respective input in one folder and run the command (we use the broader masks again, not the eroded ones! They were only important for mtnormalise).
```{r}
fod_subjects <- fod_df %>% select(LI_Nr) %>% pull() %>% unique()
template_dir <- file.path("/data/pt_02571/Data/preprocessed/template")
for(input in c("fod_input", "mask_input")){
  if(!dir.exists(file.path(template_dir, sprintf("%s", input)))){
    dir.create(file.path(template_dir,sprintf("%s", input)), recursive = TRUE)}
}

if(!dir.exists(template_dir)){dir.create(template_dir, recursive = TRUE)}

slurm_dir <- file.path(fixel_dir, "template_links_slurm")
if(!dir.exists(slurm_dir)){dir.create(slurm_dir, recursive = TRUE)}


    for(fod_subject in fod_subjects){
    
      #system(cmd)
      #write set up part of sbatch file
      
      cmd_1 <- sprintf("ln -sr %s/%s/wmfod_norm.mif %s/fod_input/%s_PRE.mif", 
                     path, fod_subject, template_dir, fod_subject)
      cmd_2 <- sprintf("ln -sr %s/%s/eddy_dwi_mask_upsampled.mif %s/mask_input/%s_PRE.mif", 
                      path, fod_subject, template_dir, fod_subject)
      
      fn_sh <- file.path(slurm_dir, sprintf("%s_symbolic_links.sh", fod_subject))
      mem_req <- 16000
      tim_req <- "0-02:00"
      con <- file(fn_sh)
      open(con, "w")
            writeLines(c(sprintf("#!/bin/bash\n#"),
                         sprintf("#SBATCH -n 4"),
                         sprintf("#SBATCH -N 1"),
                         sprintf("#SBATCH --mem=%d", mem_req),
                         sprintf("#SBATCH --time=%s", tim_req),
                         sprintf("#SBATCH --job-name %s_link", fod_subject),
                         sprintf("#SBATCH --mail-type=begin"),
                         sprintf("#SBATCH --mail-type=end"),
                         sprintf("#SBATCH --mail-type=fail"),
                         sprintf("#SBATCH --mail-user=%s@cbs.mpg.de", system("echo $USER", 
                                                                             intern = TRUE)),
            sprintf("#SBATCH -o %s/%s_mtnorm.out",
                    slurm_dir, fod_subject),
            sprintf("#SBATCH -e %s/%s_mtnorm.err",
                    slurm_dir, fod_subject)
            ), con)
  
            # write the film_gls command to the sbatch file
            writeLines(cmd_1, con, sep = "\n\n")
            writeLines(cmd_2, con, sep = "\n\n")
  
            # close connection and make sbatch file executable
            close(con)
          system(sprintf("chmod +x %s",fn_sh))
  
          # submit to slurm
          system(sprintf("sbatch %s",fn_sh))
    }


```

Only after above links are created we make the actual template:

```{r}
fod_input_dir <- file.path(template_dir, "fod_input")
if(!dir.exists(fod_input_dir)){dir.create(fod_input_dir, recursive = TRUE)}
mask_input_dir <- file.path(template_dir, "mask_input")
if(!dir.exists(mask_input_dir)){dir.create(mask_input_dir, recursive = TRUE)}

slurm_dir <- file.path(fixel_dir, "fod_template_slurm")
if(!dir.exists(slurm_dir)){dir.create(slurm_dir, recursive = TRUE)}
# the command

cmd <- "population_template /data/pt_02571/Data/preprocessed/template/fod_input -mask_dir /data/pt_02571/Data/preprocessed/template/mask_input /data/pt_02571/Data/preprocessed/template/wmfod_template.mif -voxel_size 1.25"
mem_req <- 500000
tim_req <- "1-10:00"
fn_sh <- file.path(slurm_dir, "fod_template.sh")
con <- file(fn_sh)
open(con, "w")
writeLines(c(sprintf("#!/bin/bash\n#"),
                    sprintf("#SBATCH -n 4"),
                    sprintf("#SBATCH -N 1"),
                    sprintf("#SBATCH --mem=%d", mem_req),
                    sprintf("#SBATCH --time=%s", tim_req),
                    sprintf("#SBATCH --job-name fod_temp"),
                    sprintf("#SBATCH --mail-type=begin"),
                    sprintf("#SBATCH --mail-type=end"),
                    sprintf("#SBATCH --mail-type=fail"),
                    sprintf("#SBATCH --mail-user=%s@cbs.mpg.de", system("echo $USER", intern = TRUE)),
                    sprintf("#SBATCH -o %s/fod_temp.out", 
                             slurm_dir),
                    sprintf("#SBATCH -e %s/fod_temp.err\n", 
                             slurm_dir)
                     ), con)
        
        # write the film_gls command to the sbatch file 
        writeLines(cmd, con, sep = "\n\n")
        
        # close connection and make sbatch file executable
        close(con)
        system(sprintf("chmod +x %s",fn_sh))
        
        # submit to slurm
        system(sprintf("sbatch %s",fn_sh))

```

## Registration to template space
```{r}
slurm_dir <- file.path(fixel_dir, "registration_slurm")
if(!dir.exists(slurm_dir)){dir.create(slurm_dir, recursive = TRUE)}
subjects <- select_df %>% filter(!(LI_Nr %in% c("LI08862673"))) %>% 
  select(LI_Nr) %>% pull
mem_req <- 16000
tim_req <- "0-02:00"
for(subject in subjects){
  cmd <- sprintf("mrregister %s/%s/wmfod_norm.mif -mask1 %s/%s/eddy_dwi_mask_upsampled.mif /data/pt_02571/Data/preprocessed/template/wmfod_template.mif -nl_warp %s/%s/subject2template_warp.mif %s/%s/template2subject_warp.mif -force", path, subject, path, subject, path, subject, path, subject)
  fn_sh <- file.path(slurm_dir, sprintf("%s_mrregister.sh", subject))
  
  
  con <- file(fn_sh)
  open(con, "w")
  writeLines(c(sprintf("#!/bin/bash\n#"),
                      sprintf("#SBATCH -n 4"),
                      sprintf("#SBATCH -N 1"),
                      sprintf("#SBATCH --mem=%d", mem_req),
                      sprintf("#SBATCH --time=%s", tim_req),
                      sprintf("#SBATCH --job-name %s_reg", subject),
                      sprintf("#SBATCH --mail-type=begin"),
                      sprintf("#SBATCH --mail-type=end"),
                      sprintf("#SBATCH --mail-type=fail"),
                      sprintf("#SBATCH --mail-user=%s@cbs.mpg.de", system("echo $USER", intern = TRUE)),
                      sprintf("#SBATCH -o %s/%s_registration.out", 
                               slurm_dir, subject),
                      sprintf("#SBATCH -e %s/%s_registration.err\n", 
                               slurm_dir, subject)
                       ), con)
          
          # write the film_gls command to the sbatch file 
          writeLines(cmd, con, sep = "\n\n")
          
          # close connection and make sbatch file executable
          close(con)
          system(sprintf("chmod +x %s",fn_sh))
          
          # submit to slurm
          system(sprintf("sbatch %s",fn_sh))
}
        
```

## Mask intersection
```{r}
path <- "/data/pt_02571/Data/preprocessed/nipype/diffusion"
slurm_dir <- file.path("/data/pt_02571/Data/preprocessed", "sbatch", "diffusion")
subjects <- select_df %>% filter(!(LI_Nr %in% c("LI08862673"))) %>% 
  select(LI_Nr) %>% pull
cmd <- sprintf("mrtransform %s/%s/eddy_dwi_mask_upsampled.mif -warp %s/%s/subject2template_warp.mif -interp nearest -datatype bit %s/%s/dwi_mask_in_template_space.mif -force", path, subjects, path, subjects, path, subjects)
fn_sh <- file.path(slurm_dir, sprintf("11_a_mask_to_template.sh"))
con <- file(fn_sh)
open(con, "w")

# write the film_gls command to the sbatch file
writeLines(cmd, con, sep = "\n\n")

# close connection and make sbatch file executable
close(con)


path <- "/data/pt_02571/Data/preprocessed/nipype/diffusion"
slurm_dir <- file.path("/data/pt_02571/Data/preprocessed", "sbatch", "diffusion")
subjects <- select_df %>% filter(!(LI_Nr %in% c("LI08862673"))) %>% 
  select(LI_Nr) %>% pull
masks <- paste(sprintf("%s/%s/dwi_mask_in_template_space.mif", path, subjects), collapse = " ")
cmd <- sprintf("mrmath %s min /data/pt_02571/Data/preprocessed/template/template_mask.mif -datatype bit -force",masks)
fn_sh <- file.path(slurm_dir, sprintf("11_b_mask_intersection.sh"))
con <- file(fn_sh)
open(con, "w")

# write the film_gls command to the sbatch file
writeLines(cmd, con, sep = "\n\n")

# close connection and make sbatch file executable
close(con)
```

Next is to be sorted:

Do the following command: fod2fixel -mask ../template/template_mask.mif -fmls_peak_value 0.06 ../template/wmfod_template.mif ../template/fixel_mask
Then: FOD segmentation without reorientation
```{r}
subjects <- select_df %>% filter(!(LI_Nr %in% c("LI08862673"))) %>% 
  select(LI_Nr) %>% pull
root_dir <- "/data/pt_02571/Data/preprocessed"
sub_dir <- file.path(root_dir, "nipype", "diffusion") 
slurm_dir <- file.path(root_dir, "sbatch", "diffusion")
template_dir <- file.path(root_dir, "template")

cmd <- sprintf("mrtransform %s/%s/wmfod_norm.mif -warp %s/%s/subject2template_warp.mif -reorient_fod no %s/%s/fod_in_template_space_NOT_REORIENTED.mif -force",sub_dir,  subjects,
    sub_dir,subjects,sub_dir, subjects)
fn_sh <- file.path(slurm_dir, sprintf("13_fod_not_reorient.sh"))
con <- file(fn_sh)
open(con, "w")

# write the film_gls command to the sbatch file
writeLines(cmd, con, sep = "\n\n")

# close connection and make sbatch file executable
close(con)


cmd <- sprintf("fod2fixel -mask %s/template_mask.mif %s/%s/fod_in_template_space_NOT_REORIENTED.mif %s/%s/fixel_in_template_space_NOT_REORIENTED -afd fd.mif -force",template_dir, 
    sub_dir,subjects,sub_dir, subjects)
fn_sh <- file.path(slurm_dir, sprintf("14_FOD_FD.sh"))
con <- file(fn_sh)
open(con, "w")

# write the film_gls command to the sbatch file
writeLines(cmd, con, sep = "\n\n")

# close connection and make sbatch file executable
close(con)



cmd <- sprintf("fixelreorient %s/%s/fixel_in_template_space_NOT_REORIENTED %s/%s/subject2template_warp.mif %s/%s/fixel_in_template_space -force", sub_dir, subjects, 
    sub_dir,subjects,sub_dir, subjects)
fn_sh <- file.path(slurm_dir, sprintf("15_FOD_FD_reorient.sh"))
con <- file(fn_sh)
open(con, "w")

# write the film_gls command to the sbatch file
writeLines(cmd, con, sep = "\n\n")

# close connection and make sbatch file executable
close(con)

cmd <- sprintf("fixelcorrespondence %s/%s/fixel_in_template_space/fd.mif %s/fixel_mask %s/fd %s_PRE.mif", sub_dir, subjects, template_dir, template_dir, subjects)
fn_sh <- file.path(slurm_dir, sprintf("16_FOD_fixel_match.sh"))
con <- file(fn_sh)
open(con, "w")

# write the film_gls command to the sbatch file
writeLines(cmd, con, sep = "\n\n")

# close connection and make sbatch file executable
close(con)

cmd <- sprintf("warp2metric %s/%s/subject2template_warp.mif -fc %s/fixel_mask %s/fc %s.mif", sub_dir, subjects, template_dir, template_dir, subjects)
fn_sh <- file.path(slurm_dir, sprintf("17_FC.sh"))
con <- file(fn_sh)
open(con, "w")

# write the film_gls command to the sbatch file
writeLines(cmd, con, sep = "\n\n")

# close connection and make sbatch file executable
close(con)

cmd <- sprintf("mrcalc %s/fc/%s.mif -log %s/log_fc/%s.mif", template_dir, subjects, template_dir, subjects)
fn_sh <- file.path(slurm_dir, sprintf("17_b_log_FC.sh"))
con <- file(fn_sh)
open(con, "w")

# write the film_gls command to the sbatch file
writeLines(cmd, con, sep = "\n\n")

# close connection and make sbatch file executable
close(con)

cmd <- sprintf("mrcalc %s/fd/%s_PRE.mif %s/fc/%s.mif -mult %s/fdc/%s.mif", template_dir, subjects, template_dir, subjects, template_dir, subjects)
fn_sh <- file.path(slurm_dir, sprintf("18_FDC.sh"))
con <- file(fn_sh)
open(con, "w")

# write the film_gls command to the sbatch file
writeLines(cmd, con, sep = "\n\n")

# close connection and make sbatch file executable
close(con)
``` 

##fixel-connectivity
To be executed within .../template folder
```{r}
cmd <- "tckgen -angle 22.5 -maxlen 250 -minlen 10 -power 1.0 wmfod_template.mif -seed_image template_mask.mif -mask template_mask.mif -select 20000000 -cutoff 0.06 tracks_20_million.tck"
slurm_dir <- file.path("/data/pt_02571/Data/preprocessed", "sbatch", "diffusion")
mem_req <- 100000
tim_req <- "2-20:00"
fn_sh <- file.path(slurm_dir, "20m_fixel_connectivity.sh")
con <- file(fn_sh)
open(con, "w")
writeLines(c(sprintf("#!/bin/bash\n#"),
                    sprintf("#SBATCH -n 4"),
                    sprintf("#SBATCH -N 1"),
                    sprintf("#SBATCH --mem=%d", mem_req),
                    sprintf("#SBATCH --time=%s", tim_req),
                    sprintf("#SBATCH --job-name 20m_tract"),
                    sprintf("#SBATCH --mail-type=begin"),
                    sprintf("#SBATCH --mail-type=end"),
                    sprintf("#SBATCH --mail-type=fail"),
                    sprintf("#SBATCH --mail-user=%s@cbs.mpg.de", system("echo $USER", intern = TRUE)),
                    sprintf("#SBATCH -o %s/20m_tract.out", 
                             slurm_dir),
                    sprintf("#SBATCH -e %s/20m_tract.err\n", 
                             slurm_dir)
                     ), con)
        
        # write the film_gls command to the sbatch file 
        writeLines(cmd, con, sep = "\n\n")
        
        # close connection and make sbatch file executable
        close(con)
        #system(sprintf("chmod +x %s",fn_sh))
        
        # submit to slurm
        #system(sprintf("sbatch %s",fn_sh))
```



```{r}
cmd_1 <- "tcksift tracks_20_million.tck wmfod_template.mif tracks_2_million_sift.tck -term_number 2000000"
cmd_2 <- "fixelconnectivity fixel_mask/ tracks_2_million_sift.tck matrix/"
mem_req <- 100000
tim_req <- "1-20:00"
fn_sh <- file.path(slurm_dir, "20_21_fixel_connectivity.sh")
con <- file(fn_sh)
open(con, "w")
writeLines(c(sprintf("#!/bin/bash\n#"),
                    sprintf("#SBATCH -n 4"),
                    sprintf("#SBATCH -N 1"),
                    sprintf("#SBATCH --mem=%d", mem_req),
                    sprintf("#SBATCH --time=%s", tim_req),
                    sprintf("#SBATCH --job-name fod_temp"),
                    sprintf("#SBATCH --mail-type=begin"),
                    sprintf("#SBATCH --mail-type=end"),
                    sprintf("#SBATCH --mail-type=fail"),
                    sprintf("#SBATCH --mail-user=%s@cbs.mpg.de", system("echo $USER", intern = TRUE)),
                    sprintf("#SBATCH -o %s/sift.out", 
                             slurm_dir),
                    sprintf("#SBATCH -e %s/sift.err\n", 
                             slurm_dir)
                     ), con)
        
        # write the film_gls command to the sbatch file 
        writeLines(cmd_1, con, sep = "\n")
        writeLines(cmd_2, con, sep = "\n\n")
        
        # close connection and make sbatch file executable
        close(con)
        #system(sprintf("chmod +x %s",fn_sh))
        
        # submit to slurm
        #system(sprintf("sbatch %s",fn_sh))
```


smoothing commands...

We would finally like to use ROI based analyses - based on our whole-brain tractography. 
We will follow the steps described in this post: 
https://community.mrtrix.org/t/roi-based-fixel-based-analysis/6246/5

We might want to get a better qualitiy by using ANTS (since I don't have much experience with it, not sure if I want to...):
https://community.mrtrix.org/t/warping-functional-rois-from-mni-space-to-wmfod-template-space/3104/2

see also his post in the wiki:
https://community.mrtrix.org/t/registration-using-transformations-generated-from-other-packages/2259


And we also might want to have a look at the post of Smith: 
https://community.mrtrix.org/t/use-of-volumetric-templates-parcellation-atlases/2253



```{r}
cmd <- c("flirt -ref ../MNI152_T1_1mm_brain.nii.gz -in I0image.nii.gz -omat template2mni.mat -dof 12", 
         "fnirt --ref=../MNI152_T1_1mm_brain.nii.gz --in=I0image.nii.gz --aff=template2mni.mat--cout=warps_template2mni",
         "invwarp --ref=I0image.nii.gz --warp=warps_template2mni --out=warps_mni2template",
         "warpinit ../MNI152_T1_1mm_brain.nii.gz inv_identity_warp_no_mni.nii",
         "applywarp --ref=I0image.nii.gz --in=inv_identity_warp_no_mni.nii --warp=warps_mni2template.nii.gz --out=mrtrix_warp_mni2template.nii.gz",
         "mrtransform ../BN_Atlas_246_1mm.nii.gz -warp mrtrix_warp_mni2template.nii.gz bna246toTemplate_mni.nii.gz -template wmfod_template.mif -interp nearest",
         "mrconvert bna246toTemplate_mni.nii.gz bna246toTemplate_mni.mif")
```

