Welcome! 

Here you can find the script of all statistical analyses. The corresponding code is located in the "code" folder. In case you would like to contribute, add a new chapter by creating a Markdown document within the code folder. For additional resources how you create books with bookdown  (https://github.com/rstudio/bookdown) see

The **bookdown** book: https://bookdown.org/yihui/bookdown/

The **bookdown** package reference site: https://pkgs.rstudio.com/bookdown
