%% Analysis file written by Lina Schaare, schaare@cbs.mpg.de
% ANT file (MRI compatible)
% adapted by Marie Meemken, meemken@cbs.mpg.de for use in the longCOVID
% project NRO-TK264

%% version history
% written by schaare@cbs.mpg.de and sent to GutBrain study in 2020
% adapted by meemken@cbs.mpg.de on 2023-06-23

%% Start script
clear all; clc;

sub_list = [1:2]; %%% adapt number of subject IDs you want to analyze
logdir = '/data/p_02571/ant/LC2/';
outdir = '/data/pt_02571/Data/ant/LC2/';

%***LOADING LOGFILE***
% Find all logfiles in logdir
fDir = dir([logdir,'*-ant1.*.log']);
fDir = struct2cell(fDir);

% stable information for all files
conditions = {'neut_nocue','cong_nocue','incong_nocue','neut_ccue','cong_ccue','incong_ccue','neut_dcue','cong_dcue','incong_dcue','neut_scue','cong_scue','incong_scue'};
response_button_l = 1; %%% left
response_button_r = 2; %% right

% June 2023 - add table with sample output (1)
% (1) Create table
%% SET UP A TABLE FOR ALL DATA! 12 errors, 12 RTs per participant
sampledata = table() 


% loop through all individual files
for l = 1 : 1%: size(fDir,2)
    % filename -> read content of file and save in cell structure
    fName = fDir{1,l};
    fid = fopen(fullfile(logdir,fName));
    C   = textscan(fid, '%s', 'delimiter', '\n');
    fclose(fid);

    % get SubjectID for foldername
    line = char(C{1}(6));
    lineComponents = textscan(line, '%s %s %s %s %s %s %s', 'delimiter', ';');
    SubjectID = char(lineComponents{1}(1));
    
    % route to output directory 
    cd(outdir); % change current directory
    mkdir(SubjectID);   % create folder for outputs

    lDir = [pwd, filesep(), '' ];
    % RT-file for reaction times
    filename = sprintf('%s/ANT_RTs.txt', SubjectID);
    rt   = fopen(fullfile(lDir,filename), 'w');
    % errors file for errors
    filename = sprintf('%s/ANT_errors.txt', SubjectID);
    errors  = fopen(fullfile(lDir,filename), 'w'); 

    %% Task Data
    waitforresponse = false;
    flanker = [];
    RT = cell(3,4); %% Response Time
    ERR = cell(3,4); %% Error Rate
    begin = false;
    %Run 1
    for i=6:size(C{1},1)

        %Read
        line = char(C{1}(i));
        lineComponents = textscan(line, '%s %s %s %s %s %s %s %s %s %s %s %s %s', 'delimiter', ';');
        Subject = char(lineComponents{1}(1));
        Trial	 = char(lineComponents{2}(1));
        Event   = char(lineComponents{3}(1));
        Code	 = char(lineComponents{4}(1));
        Time    = char(lineComponents{5}(1));
        %Duration = char(lineComponents{8}(1));

        if (strcmp(Event, 'Picture') && strcmp(Code, 'Start_Exp'))
            begin = true;
        end
        if(strcmp(Event, 'Picture') && begin == true)
            %**** a pic ****

            %**** a cue ****
            if strfind(Code, 'cue:_')
                warning = Code;
                switch warning
                    case {'cue:_n'} %% none
                        cue = 1;
                    case {'cue:_c'} %%% center
                        cue = 2;
                    case {'cue:_d'} %%% double
                        cue = 3;
                    case {'cue:_s_u','cue:_s_d'} %%% spatial_up / spatial_down
                        cue = 4;
                    otherwise
                        disp(['problem with cue in line ' num2str(line)])
                end

                %**** a target ****
            elseif strfind(Code, '_flanker-cond:_')
                if(waitforresponse)
                    disp('ATTENTION: Response missed, inserting response uncorrect!!!!');
                end
                flankerTime = floor(str2num(Time)/10);
                %fprintf(onsets, '%d\n', flankerTime); ### delete
                if strfind(Code, '_flanker-cond:_neut_')
                    flanker = 1;
                elseif strfind(Code, '_flanker-cond:_cong_')
                    flanker = 2;
                elseif strfind(Code, '_flanker-cond:_incong_')
                    flanker = 3;
                else
                    disp(['problem with flanker in line ' num2str(line)])
                end

                if strfind(Code, '_target-dir:_r')
                    target = 2;
                elseif strfind(Code, '_target-dir:_l')
                    target = 1;
                else
                    disp(['problem with target direction in line ' num2str(line)])
                end
                % comment out for future analyses with corrected presentation script
                if contains(Code, '_flanker-cond:_incong_') && contains(Code, '_target-pos:_u') && contains(Code, '_target-dir:_r')
                    target = 2;
                    % disp(Code)
                    % disp(num2str(target))
                elseif contains(Code, '_flanker-cond:_incong_') && contains(Code, '_target-pos:_u') && contains(Code, '_target-dir:_l')
                    target = 1;
                    % disp(Code)
                    % disp(num2str(target))
                elseif contains(Code, '_flanker-cond:_incong_') && contains(Code, '_target-pos:_d') && contains(Code, '_target-dir:_r')
                    target = 1;
                    % disp(Code)
                    % disp(num2str(target))
                elseif contains(Code, '_flanker-cond:_incong_') && contains(Code, '_target-pos:_d') && contains(Code, '_target-dir:_l')
                    target = 2;
                    % disp(Code)
                    % disp(num2str(target))
                end
                % end of comment
                waitforresponse = true;
            end
        end


        %**** a response ****
        if (strcmp(Event, 'Response') && waitforresponse)
            waitforresponse = false;
            respValue = str2num(Code);
            % if button press was correct
            if (respValue == response_button_l && target == 1) == 1 ...
                    || (respValue == response_button_r && target == 2) == 1
                responseCorrect = 1;
                respTime = floor(str2num(Time)/10);
                reactionTime = respTime-flankerTime;
                RT{flanker,cue} = [RT{flanker,cue},reactionTime];
            % if button press was incorrect
            else
                responseCorrect = 0;
                ERR{flanker,cue} = [ERR{flanker,cue},1];
                disp('error')
            end

        end
    end


    for c = 1:12
        meanrt = mean(RT{c});
        disp(meanrt)
        fprintf(rt, '%s %4.3f\n', conditions{c}, meanrt);
        err_per = sum(ERR{c})/24*100;
        fprintf(errors, '%s %3.2f\n', conditions{c}, err_per);
        % June 2023 - add table with sample output (2)
        % (2) enter participant data in sample table
        %% ID, 12 errors, 12 RTs per participant
    end

    fclose(rt);
    fclose(errors);
end
