#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon 08 01 2024

@author: fbeyer
"""
import sys
import os
import pandas as pd
import subprocess
'''
Meta script to perform fixel2voxel transformation 
following https://community.mrtrix.org/t/fixel2voxel-how-to-extract-primary-secondary-and-tertiary-voxel-based-fd-maps/4394/10:
fd and fdc maps for each subject using fixel2voxel with the “sum” option, and log_fc values with the mean option, weighted by the mean FD map across all subjects.
except that we will use the invidivual fiber density map and registration to FA template using previously estimated Ants-Transforms
---------------------------------------------------
'''
# slurm resources
user="fbeyer"
slurm_dir = "/data/pt_02571/Analysis/longcovid/TBSS//slurm/"
mem_req = 6000 # adjust accordingly
tim_req = "1-00:00" # days-hours:minutes. Current max time is 7 days


df=pd.read_excel("/data/pt_02571/Data/Tables/Samples_LIFE.xlsx")
subjects=df[(df["DICOM"]>193)&(df["LI_Nr"]!="LI08864576")]["LI_Nr"]

print(len(subjects)) #125 subjects

for index, subject in enumerate(subjects):
   
    cmd0 = "fixel2voxel /data/pt_02571/Data/preprocessed/template/fd_smooth/{}_PRE.mif sum /data/pt_02571/Data/preprocessed/template/fd_smooth/{}_sum_fd.nii".format(subject, subject)
    cmd1= "fixel2voxel /data/pt_02571/Data/preprocessed/template/log_fc_smooth/{}.mif mean -weighted /data/pt_02571/Data/preprocessed/template/fd_smooth/{}_PRE.mif /data/pt_02571/Data/preprocessed/template/log_fc_smooth/{}_mean_log_fc.nii".format(subject, subject, subject)
    cmd2= "fixel2voxel /data/pt_02571/Data/preprocessed/template/fdc_smooth/{}.mif sum /data/pt_02571/Data/preprocessed/template/fdc_smooth/{}_sum_fdc.nii".format(subject, subject)
    
    cmd3= "antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/dtifit__MD.nii.gz  --input-image-type 3 --interpolation Linear --output /data/pt_02571/Data/TBSS/MD/{}_dtifit__MD_at_t.nii.gz --reference-image /data/pt_02571/Data/TBSS/dtifit__FA_at_t_mean.nii.gz --transform /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t1Warp.nii.gz --transform /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t0GenericAffine.mat".format(subject, subject,subject, subject)
    cmd4="antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/wls_dti_FW.nii.gz  --input-image-type 3 --interpolation Linear --output /data/pt_02571/Data/TBSS/FW/{}_dtifit__FW_at_t.nii.gz --reference-image /data/pt_02571/Data/TBSS/dtifit__FA_at_t_mean.nii.gz --transform /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t1Warp.nii.gz --transform /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t0GenericAffine.mat".format(subject, subject,subject, subject)
    cmd5="antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/fwc_wls_dti_FA.nii.gz  --input-image-type 3 --interpolation Linear --output /data/pt_02571/Data/TBSS/fw_FA/{}_dtifit__fw_FA_at_t.nii.gz --reference-image /data/pt_02571/Data/TBSS/dtifit__FA_at_t_mean.nii.gz --transform /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t1Warp.nii.gz --transform /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t0GenericAffine.mat".format(subject, subject,subject, subject)
    cmd6= "antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input /data/pt_02571/Data/preprocessed/template/fd_smooth/{}_sum_fd.nii  --input-image-type 3 --interpolation Linear --output /data/pt_02571/Data/TBSS/FD/{}_sum_FD_at_t.nii.gz --reference-image /data/pt_02571/Data/TBSS/dtifit__FA_at_t_mean.nii.gz --transform /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t1Warp.nii.gz --transform /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t0GenericAffine.mat".format(subject, subject,subject, subject)
    cmd7="antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input /data/pt_02571/Data/preprocessed/template/fdc_smooth/{}_sum_fdc.nii  --input-image-type 3 --interpolation Linear --output /data/pt_02571/Data/TBSS/FDC/{}_sum_FDC_at_t.nii.gz --reference-image /data/pt_02571/Data/TBSS/dtifit__FA_at_t_mean.nii.gz --transform /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t1Warp.nii.gz --transform /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t0GenericAffine.mat".format(subject, subject,subject, subject)
    cmd8="antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input /data/pt_02571/Data/preprocessed/template/log_fc_smooth/{}_mean_log_fc.nii  --input-image-type 3 --interpolation Linear --output /data/pt_02571/Data/TBSS/log_FC/{}_mean_log_FC_at_t.nii.gz --reference-image /data/pt_02571/Data/TBSS/dtifit__FA_at_t_mean.nii.gz --transform /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t1Warp.nii.gz --transform /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t0GenericAffine.mat".format(subject, subject,subject, subject)


    my_file = "{}/{}.sh".format(slurm_dir,subject)
    with open(my_file, "w") as my_file:
        print("#!/bin/bash\n#", file = my_file)
        print("#SBATCH -n 4", file = my_file)
        print("#SBATCH -N 1", file = my_file)
        print("#SBATCH --mem={}".format(mem_req), file = my_file)
        print("#SBATCH --time={}".format(tim_req), file = my_file)
        print("#SBATCH --job-name FAreg_{}".format(subject), file = my_file)
        print("#SBATCH --mail-type=begin", file = my_file)
        print("#SBATCH --mail-type=end", file = my_file)
        print("#SBATCH --mail-type=fail", file = my_file)
        print("#SBATCH --mail-user={}@cbs.mpg.de".format(user), file = my_file)
        print("#SBATCH -o {}/{}.out".format(slurm_dir,subject), file = my_file) 
        print("#SBATCH -e {}/{}.err\n".format(slurm_dir,subject), file = my_file)
        #write workflow command to sbatch file
        #print("{}".format(cmd0), file = my_file)
        #print("{}".format(cmd1), file = my_file)
        print("{}".format(cmd2), file = my_file)
        #print("{}".format(cmd3), file = my_file)
        #print("{}".format(cmd4), file = my_file)
        #print("{}".format(cmd5), file = my_file)
        #print("{}".format(cmd6), file = my_file)
        print("{}".format(cmd7), file = my_file)  
        #print("{}".format(cmd8), file = my_file)       
       
    with open("exec.sh", "w") as exec_file:
        print("chmod +x {}/{}.sh".format(slurm_dir,subject), file = exec_file)
        print("sbatch {}/{}.sh".format(slurm_dir,subject), file = exec_file)
        
    os.system("sh exec.sh")

        
