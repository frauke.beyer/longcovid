# Steps to compare DTI and FW imaging parameters (workflow from Petersen et al.)

## Based on the whole-brain streamlines tractogram, a fixel-fixel connectivity matrix was computed 
## -> done?? (check again with Meent)

# which was then used for smoothing the fixel metrics FD, Log. FC and FDC.

fixelfilter fd smooth fd_smooth -matrix matrix/
fixelfilter log_fc smooth log_fc_smooth -matrix matrix/
fixelfilter fdc smooth fdc_smooth -matrix matrix/

## In order to derive fixel metrics on the voxel-level, they were averaged across all fixels within a
## voxel using MRtrix3’s fixel2voxel function.
cd /data/pt_02571/Data/preprocessed/template/fd/

    cmd0 = "fixel2voxel /data/pt_02571/Data/preprocessed/template/fd_smooth/{}_PRE.mif sum /data/pt_02571/Data/preprocessed/template/fd_smooth/{}_sum_fd.nii".format(subject, subject)
    cmd1= "fixel2voxel /data/pt_02571/Data/preprocessed/template/log_fc_smooth/{}.mif mean -weighted /data/pt_02571/Data/preprocessed/template/fd_smooth/{}_PRE.mif /data/pt_02571/Data/preprocessed/template/log_fc_smooth/{}_mean_log_fc.nii".format(subject, subject, subject)
    cmd2= "fixel2voxel /data/pt_02571/Data/preprocessed/template/fdc_smooth/{}.mif sum /data/pt_02571/Data/preprocessed/template/fdc_smooth/{}_sum_fdc.nii".format(subject, subject)
    
# Therefore, previously derived individual non-linear warps from native FOD to FOD template space were used to
# register FA maps to FOD template space. 

# example command:(https://community.mrtrix.org/t/registration-transformation-regridding-concepts/3684)
cd /data/pt_02571/Data/preprocessed/nipype/diffusion/

#for subj in ls -d * LI00102053/
#mrtransform dtifit__FA.nii.gz -warp subject2template_warp.mif dtifit__FA_at_t.nii.gz -template /data/pt_02571/Data/preprocessed/template/wmfod_template.mif -interp sinc

for_each -test * : mrtransform IN/dtifit__FA.nii.gz -warp IN/subject2template_warp.mif IN/dtifit__FA_at_t.nii.gz -template /data/pt_02571/Data/preprocessed/template/wmfod_template.mif -interp sinc

#warum haben 12 subjects keinen FOD WARP?? 

# These FA maps in FOD template space were then averaged 

fslmerge */dtifit__FA_at_t.nii.gz -t /data/pt_02571/Data/TBSS/dtifit__FA_at_t_cat.nii.gz 
mrmath /data/pt_02571/Data/TBSS/dtifit__FA_at_t_cat.nii.gz mean dtifit__FA_at_t_mean.nii.gz -axis 3

# and the resulting study-specific FA template 'dtifit__FA_at_t_mean.nii.gz' served as the registration target for non-
# linear transformations of FA images from native space to template space utilizing ANTs’ SyN registration.31 

## Code:
for subj in  LI00102053 #`ls -d *`
do
cd $subj

antsRegistration --collapse-output-transforms 1 --dimensionality 3 --initial-moving-transform [ /data/pt_02571/Data/TBSS/dtifit__FA_at_t_mean.nii.gz, dtifit__FA.nii.gz, 1 ] --initialize-transforms-per-stage 0 --interpolation Linear --output [ transform, transform_Warped.nii.gz, transform_InverseWarped.nii.gz ] --transform Rigid[ 0.1 ] --metric MI[ /data/pt_02571/Data/TBSS/dtifit__FA_at_t_mean.nii.gz, dtifit__FA.nii.gz, 1, 32, Regular, 0.25 ] --convergence [ 1000x500x250x100, 1e-06, 10 ] --smoothing-sigmas 3.0x2.0x1.0x0.0vox --shrink-factors 8x4x2x1 --use-histogram-matching 1 --transform Affine[ 0.1 ] --metric MI[ /data/pt_02571/Data/TBSS/dtifit__FA_at_t_mean.nii.gz, dtifit__FA.nii.gz, 1, 32, Regular, 0.25 ] --convergence [ 1000x500x250x100, 1e-06, 10 ] --smoothing-sigmas 3.0x2.0x1.0x0.0vox --shrink-factors 8x4x2x1 --use-histogram-matching 1 --transform SyN[ 0.1, 3.0, 0.0 ] --metric CC[ /data/pt_02571/Data/TBSS/dtifit__FA_at_t_mean.nii.gz, dtifit__FA.nii.gz, 1, 4, None, 1 ] --convergence [ 100x70x50x20, 1e-06, 10 ] --smoothing-sigmas 3.0x2.0x1.0x0.0vox --shrink-factors 8x4x2x1 --use-histogram-matching 1 --winsorize-image-intensities [ 0.005, 0.995 ]  --write-composite-transform 0

fslmaths /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03925611/FA2t_Warped.nii.gz -kernel 3D -ero /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03925611/FA2t_Warped_ero1.nii.gz
fslmaths /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03925611/FA2t_Warped_ero1.nii.gz -kernel 3D -ero /data/pt_02571/Data/preprocessed/nipype/diffusion/LI03925611/FA2t_Warped_ero2.nii.gz

done


## Use slurm getserver -sb run-registration-to_FA-template.py (with environment py3) for N=127
# The resulting transformations were subsequently applied to the remaining maps
# of diffusion tensor imaging and free-water imaging metrics

for subj in `ls -d *`
do
cd $subj
antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input dtifit__MD.nii.gz  --input-image-type 3 --interpolation Linear --output dtifit__MD_at_t.nii.gz --reference-image /data/pt_02571/Data/TBSS/dtifit__FA_at_t_mean.nii.gz --transform /data/pt_02571/Data/preprocessed/template/transform2MNI/transform0GenericAffine.mat --transform transformWarp.nii.gz 

antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input wls_dti_FW.nii.gz  --input-image-type 3 --interpolation Linear --output dtifit__FW_at_t.nii.gz --reference-image dtifit__FA_at_t_mean.nii.gz --transform /data/pt_02571/Data/preprocessed/template/transform2MNI/transform0GenericAffine.mat --transform transformWarp.nii.gz 

antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input fwc_wls_dti_FA  --input-image-type 3 --interpolation Linear --output fwc_FA_at_t.nii.gz --reference-image dtifit__FA_at_t_mean.nii.gz --transform /data/pt_02571/Data/preprocessed/template/transform2MNI/transform0GenericAffine.mat --transform transformWarp.nii.gz 

done

# merge all coregistered files
cd /data/pt_02571/Analysis/longcovid/TBSS/
fslmerge -t /data/pt_02571/Data/preprocessed/nipype/diffusion/*/FA2t_Warped_ero2.nii.gz /data/pt_02571/Analysis/longcovid/TBSS/all_FA.nii.gz
fslmerge -t /data/pt_02571/Data/preprocessed/nipype/diffusion/*/dtifit__MD_at_t.nii.gz /data/pt_02571/Analysis/longcovid/TBSS/all_dtifit__MD_at_t.nii.gz
$FSLDIR/bin/fslmaths all_dtifit__MD_at_t.nii.gz -mas mean_FA_mask all_dtifit__MD_at_t.nii.gz
fslmerge -t /data/pt_02571/Data/preprocessed/nipype/diffusion/*/fwc_FA_at_t.nii.gz /data/pt_02571/Analysis/longcovid/TBSS/all_fwc_FA_at_t.nii.gz
$FSLDIR/bin/fslmaths all_fwc_FA_at_t.nii.gz -mas mean_FA_mask all_fwc_FA_at_t.nii.gz
fslmerge -t /data/pt_02571/Data/preprocessed/nipype/diffusion/*/dtifit__FW_at_t.nii.gz /data/pt_02571/Analysis/longcovid/TBSS/all_dtifit__FW_at_t.nii.gz
$FSLDIR/bin/fslmaths all_dtifit__FW_at_t.nii.gz -mas mean_FA_mask all_dtifit__FW_at_t.nii.gz

# we conducted tract-based spatial statistics (TBSS)32,33 utilizing the above described study-specific FA
# template as the registration target. Briefly, individual FA images in template space got eroded
# to exclude non-brain voxels on the outer edge of the image (fslmaths -kernel 3D -ero ²). 

# Next, a valid mask containing only the intersection of all subjects’ brains was derived and used to mask the average of all previ-
# ously eroded FA images. 

# This mean FA image was subsequently used to derive a white matter skeleton which was thresholded at FA > 0.25. 
# Next, all individual FA images were projected
# onto the mean FA skeleton. 

# echo "creating valid mask and mean FA"

$FSLDIR/bin/fslmaths all_FA -max 0 -Tmin -bin mean_FA_mask -odt char
$FSLDIR/bin/fslmaths all_FA -mas mean_FA_mask all_FA
$FSLDIR/bin/fslmaths all_FA -Tmean mean_FA

# create skeleton + skeleton mask
$FSLDIR/bin/tbss_skeleton -i mean_FA -o mean_FA_skeleton
${FSLDIR}/bin/fslmaths mean_FA_skeleton -thr 0.25 -bin mean_FA_skeleton_mask

# echo "creating skeleton distancemap (for use in projection search)"
${FSLDIR}/bin/fslmaths mean_FA_mask -mul -1 -add 1 -add mean_FA_skeleton_mask mean_FA_skeleton_mask_dst
${FSLDIR}/bin/distancemap -i mean_FA_skeleton_mask_dst -o mean_FA_skeleton_mask_dst

# echo "projecting all FA data onto skeleton"
${FSLDIR}/bin/tbss_skeleton -i mean_FA -p 0.25 mean_FA_skeleton_mask_dst ${FSLDIR}/data/standard/LowerCingulum_1mm all_FA all_FA_skeletonised

# projecting other data onto skeleton (FW, fw-corrected FA, MD, FD, FC, FDC)
${FSLDIR}/bin/tbss_skeleton -i mean_FA -p 0.25 mean_FA_skeleton_mask_dst ${FSLDIR}/data/standard/LowerCingulum_1mm all_FA all_FW_skeletonised -a all_dtifit__FW_at_t.nii.gz
${FSLDIR}/bin/tbss_skeleton -i mean_FA -p 0.25 mean_FA_skeleton_mask_dst ${FSLDIR}/data/standard/LowerCingulum_1mm all_FA all_fw_FA_skeletonised -a all_fwc_FA_at_t.nii.gz
${FSLDIR}/bin/tbss_skeleton -i mean_FA -p 0.25 mean_FA_skeleton_mask_dst ${FSLDIR}/data/standard/LowerCingulum_1mm all_FA all_MD_skeletonised -a all_dtifit__MD_at_t.nii.gz
${FSLDIR}/bin/tbss_skeleton -i mean_FA -p 0.25 mean_FA_skeleton_mask_dst ${FSLDIR}/data/standard/LowerCingulum_1mm all_FA all_FD_skeletonised -a all_FD
${FSLDIR}/bin/tbss_skeleton -i mean_FA -p 0.25 mean_FA_skeleton_mask_dst ${FSLDIR}/data/standard/LowerCingulum_1mm all_FA all_FC_skeletonised -a all_FC
${FSLDIR}/bin/tbss_skeleton -i mean_FA -p 0.25 mean_FA_skeleton_mask_dst ${FSLDIR}/data/standard/LowerCingulum_1mm all_FA all_FDC_skeletonised -a all_FDC

# The resultant projection vectors were used to skeletonize all of the
# remaining diffusion metrics including those from free-water imaging and fixel-based analysis
# pipelines. Finally, diffusion markers were averaged across the entire white matter skeleton for
# further statistical analysis
