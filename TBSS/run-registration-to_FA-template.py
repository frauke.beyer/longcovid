#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon 08 01 2024

@author: fbeyer
"""
import sys
import os
import pandas as pd
import subprocess
'''
Meta script to run registration to FA template
---------------------------------------------------
'''
# slurm resources
user="fbeyer"
slurm_dir = "/data/pt_02571/Analysis/longcovid/TBSS//slurm/"
mem_req = 6000 # adjust accordingly
tim_req = "1-00:00" # days-hours:minutes. Current max time is 7 days


df=pd.read_excel("/data/pt_02571/Data/Tables/Samples_LIFE.xlsx")
subjects=df[(df["DICOM"]>0)]["LI_Nr"]

print(len(subjects)) #127 subjects

for index, subject in enumerate(subjects):
    cmd = "antsRegistration --collapse-output-transforms 1 --dimensionality 3 --initial-moving-transform [ /data/pt_02571/Data/TBSS/dtifit__FA_at_t_mean.nii.gz, /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/dtifit__FA.nii.gz, 1 ] --initialize-transforms-per-stage 0 --interpolation Linear --output [ /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t, /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t_Warped.nii.gz, /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t_InverseWarped.nii.gz ] --transform Rigid[ 0.1 ] --metric MI[ /data/pt_02571/Data/TBSS/dtifit__FA_at_t_mean.nii.gz, /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/dtifit__FA.nii.gz, 1, 32, Regular, 0.25 ] --convergence [ 1000x500x250x100, 1e-06, 10 ] --smoothing-sigmas 3.0x2.0x1.0x0.0vox --shrink-factors 8x4x2x1 --use-histogram-matching 1 --transform Affine[ 0.1 ] --metric MI[ /data/pt_02571/Data/TBSS/dtifit__FA_at_t_mean.nii.gz, /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/dtifit__FA.nii.gz, 1, 32, Regular, 0.25 ] --convergence [ 1000x500x250x100, 1e-06, 10 ] --smoothing-sigmas 3.0x2.0x1.0x0.0vox --shrink-factors 8x4x2x1 --use-histogram-matching 1 --transform SyN[ 0.1, 3.0, 0.0 ] --metric CC[ /data/pt_02571/Data/TBSS/dtifit__FA_at_t_mean.nii.gz, /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/dtifit__FA.nii.gz, 1, 4, None, 1 ] --convergence [ 100x70x50x20, 1e-06, 10 ] --smoothing-sigmas 3.0x2.0x1.0x0.0vox --shrink-factors 8x4x2x1 --use-histogram-matching 1 --winsorize-image-intensities [ 0.005, 0.995 ]  --write-composite-transform 0".format(subject, subject, subject,subject, subject, subject, subject)
    cmd2= "fslmaths /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t_Warped.nii.gz -kernel 3D -ero /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t_Warped_ero1.nii.gz".format(subject, subject)
    cmd3= "fslmaths /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t_Warped_ero1.nii.gz -kernel 3D -ero /data/pt_02571/Data/preprocessed/nipype/diffusion/{}/FA2t_Warped_ero2.nii.gz".format(subject, subject)
    
    my_file = "{}/{}.sh".format(slurm_dir,subject)
    with open(my_file, "w") as my_file:
        print("#!/bin/bash\n#", file = my_file)
        print("#SBATCH -n 4", file = my_file)
        print("#SBATCH -N 1", file = my_file)
        print("#SBATCH --mem={}".format(mem_req), file = my_file)
        print("#SBATCH --time={}".format(tim_req), file = my_file)
        print("#SBATCH --job-name FAreg_{}".format(subject), file = my_file)
        print("#SBATCH --mail-type=begin", file = my_file)
        print("#SBATCH --mail-type=end", file = my_file)
        print("#SBATCH --mail-type=fail", file = my_file)
        print("#SBATCH --mail-user={}@cbs.mpg.de".format(user), file = my_file)
        print("#SBATCH -o {}/{}.out".format(slurm_dir,subject), file = my_file) 
        print("#SBATCH -e {}/{}.err\n".format(slurm_dir,subject), file = my_file)
        #write workflow command to sbatch file
        print("{}".format(cmd), file = my_file)
        print("{}".format(cmd2), file = my_file)
        print("{}".format(cmd3), file = my_file)
        
       
    with open("exec.sh", "w") as exec_file:
        print("chmod +x {}/{}.sh".format(slurm_dir,subject), file = exec_file)
        print("sbatch {}/{}.sh".format(slurm_dir,subject), file = exec_file)
        
    os.system("sh exec.sh")

        
